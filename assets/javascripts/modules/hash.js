// **************
// GENERATE HASH
// **************

(function() {

    "use strict";

    // button to generate hash
    var buttonToHash = document.getElementById('buttonToHash');
    if(buttonToHash){
        buttonToHash.addEventListener('click', function(e){
            
            var errorHash = document.getElementById("errorHash");
            errorHash.innerHTML = '';

            var strToHash = document.getElementById("strToHash").value;
            if('' == strToHash) {                           
                document.getElementById("errorHash").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('various.indicateChainToHash') + '</p>'
                return;
            }
            generateHash();
            window.scrollBy(0, 400);
        });
    }

    // call the SHA "X"() passing the strToHash (input entered by the user), this method returns the hash of the text passed to it.
    function generateHash() {
        function separatorEveryNChar(str, separator, nChar) {
            var chain = '';
            var resultTab = [];
            for (var i=0; i < str.length; i++) {
                chain += str[i];

                if(chain.length % nChar === 0) {
                    var temp = chain.slice(chain.length - nChar);
                    resultTab.push(temp);
                }
            }
            return resultTab.join(separator);
        }
        
        var strToHash = document.getElementById("strToHash").value;

        var hashSHA512 = CryptoJS.SHA512(strToHash).toString().toUpperCase();
        var hashSHA384 = CryptoJS.SHA384(strToHash).toString().toUpperCase();
        var hashSHA256 = CryptoJS.SHA256(strToHash).toString().toUpperCase();
        var hashSHA1 = CryptoJS.SHA1(strToHash).toString().toUpperCase();
        var hashMD5 = CryptoJS.MD5(strToHash).toString().toUpperCase();

        var separatedHashSHA512 = separatorEveryNChar(hashSHA512, '&nbsp;&nbsp;', 4);
        var separatedHashSHA384 = separatorEveryNChar(hashSHA384, '&nbsp;&nbsp;', 4);
        var separatedHashSHA256 = separatorEveryNChar(hashSHA256, '&nbsp;&nbsp;', 4);
        var separatedHashSHA1 = separatorEveryNChar(hashSHA1, '&nbsp;&nbsp;', 4);
        var separatedHashMD5 = separatorEveryNChar(hashMD5, '&nbsp;&nbsp;', 4);


        document.getElementById("hashText").innerHTML = 
        '<hr><strong>SHA-512</strong><br> ' + hashSHA512
        + '<br><br><p><i class="text-muted">' + i18next.t('various.blockOf4') + '</i><br> ' + separatedHashSHA512 + '</p>'

        + '<hr><strong>SHA-384</strong><br> ' + hashSHA384
        + '<br><br><p><i class="text-muted">' + i18next.t('various.blockOf4') + '</i><br> ' + separatedHashSHA384 + '</p>'

        + '<hr><strong>SHA-256</strong><br> ' + hashSHA256
        + '<br><br><p><i class="text-muted">' + i18next.t('various.blockOf4') + '</i><br> ' + separatedHashSHA256 + '</p>'

        + '<hr><strong>SHA-1</strong><br> ' + hashSHA1
        + '<br><br><p><i class="text-muted">' + i18next.t('various.blockOf4') + '</i><br> ' + separatedHashSHA1 + '</p>'
        
        + '<hr><strong>MD5</strong><br> ' + hashMD5
        + '<br><br><p><i class="text-muted">' + i18next.t('various.blockOf4') + '</i><br> ' + separatedHashMD5 + '</p>';
        
        var hashToCompare = document.getElementById("hashToCompare").value;
        if(hashToCompare != '') {
            if (hashToCompare.toString().toLowerCase() === hashSHA512.toString().toLowerCase()) {
                var status = 'success';
                var text = '<i class="bi bi-file-check"></i> ' + i18next.t('error.hashWellCalculated');
            } else {
                var status = 'danger';
                var text = '<i class="bi bi-exclamation-triangle"></i> ' + i18next.t('error.hashNotCorresponding');
            }
            document.getElementById("result").innerHTML = '<div class="alert alert-' + status + '">'+ text + '</div>';
        }
    }
})();
