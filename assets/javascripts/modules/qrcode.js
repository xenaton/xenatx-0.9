// *****************
// GENERATE QR CODE
// *****************

// ****
// EMIT
// ****
(function() {

    "use strict";

    // qrcode character counter
    var displayEnteredCharQRC = document.getElementById("displayEnteredCharQRC");  
    var variousContent = document.getElementById("variousContent");   
    const qrcodeCharCounter = () => {
        let numOfEnteredChars = variousContent.value.length;
        displayEnteredCharQRC.textContent = numOfEnteredChars;
    };
    variousContent.addEventListener("input", qrcodeCharCounter);

    // multiples QR Codes counter
    function addNewQrCode(index, item, multiple) {
        var qrcodeWrapper = document.getElementById('qrcodeWrapper');
        var hash = CryptoJS.SHA1(item).toString().toUpperCase();

        var newDivTitle = document.createElement('p');
        var newDivSubTitle = document.createElement('p');
        newDivTitle.setAttribute("class", "h3");
        newDivTitle.innerText = 'QR Code n°' + (index+1);
        newDivSubTitle.innerText = separatorEveryNChar(hash, ' ', 4) + ' (empreinte SHA-1)';
        qrcodeWrapper.appendChild(newDivTitle);
        qrcodeWrapper.appendChild(newDivSubTitle);

        // visual hash
        var visualHashCanvas = document.createElement('canvas');
        visualHashCanvas.setAttribute("id", "hashCanvas" + index);
        visualHashCanvas.setAttribute("class", "qrcode__pad");
        visualHashCanvas.setAttribute("title", 'Empreinte visuelle SHA-1 : ' + separatorEveryNChar(hash, ' ', 4));
        qrcodeWrapper.appendChild(visualHashCanvas);
        
        var canvas = document.getElementById("hashCanvas" + index);
        visualHash(canvas, hash);

        if(multiple) {
            var newDivQRCode = document.createElement('div');
            var newHr = document.createElement('hr');
            newDivQRCode.setAttribute("id", "qrcode" + index);
            newDivQRCode.setAttribute("class", "qrcode__pad");
            qrcodeWrapper.appendChild(newDivQRCode);
            qrcodeWrapper.appendChild(newHr);
        } 
    }
   
    // factory to generate QR Code
    function makeCode () {		
        // set transport capacity of QR Code
        var qrcodeCapacity = document.getElementById('qrcodeCapacity');
        if(qrcodeCapacity){
            var variousContentBlock = qrcodeCapacity.value;
        }

        var variousContent = document.getElementById('variousContent').value;
        var variousContentMaxCharacters = variousContentBlock * 100;

        // error empty
        if(variousContent == '') {
            document.getElementById("errorQRCode").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.contentToQRCodeEmpty') + '</p>'
            return;
        }

        // error too long for max number of QR Codes
        if(variousContent.length > variousContentMaxCharacters) {
            document.getElementById("errorQRCode").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.tooBigForQRCode1') + variousContentMaxCharacters + i18next.t('error.tooBigForQRCode2') + '</p>';
            return;
        }
        
        var qrcodeWrapper = document.getElementById('qrcodeWrapper');

        if(variousContent.length > variousContentBlock) {

            var blocks = splitToSubstrings(variousContent, Number(variousContentBlock));  

            // number of QR Codes
            var numberOfQRCodes = document.createElement('p');
            numberOfQRCodes.setAttribute("class", "h5");
            var numberOfBlocks = blocks.length;
            numberOfQRCodes.innerText = numberOfBlocks + ' QR Codes ont été générés pour transporter ce contenu textuel.';
            qrcodeWrapper.appendChild(numberOfQRCodes);

            // global hash
            var hash = CryptoJS.SHA1(variousContent).toString().toUpperCase();
            var globalHashDiv = document.createElement('p');
            globalHashDiv.setAttribute("class", "h4");
            globalHashDiv.innerText = 'Empreinte SHA-1 globale (textuelle et visuelle) : ' + separatorEveryNChar(hash, ' ', 4);;
            qrcodeWrapper.appendChild(globalHashDiv);

            // global visual hash
            var visualHashCanvas = document.createElement('canvas');
            visualHashCanvas.setAttribute("id", "hashCanvas");
            visualHashCanvas.setAttribute("class", "qrcode__pad");
            visualHashCanvas.setAttribute("title", "Empreinte visuelle globale SHA-1");
            qrcodeWrapper.appendChild(visualHashCanvas);
            
            var canvas = document.getElementById("hashCanvas");
            visualHash(canvas, hash, 256, 256);

            // var blocks = variousContent.match(/.{1,700}/g);
            // equivalent of match above but with ability to use variables (too hard to place vars in match function)
            function splitToSubstrings(str, nChar) {
                const arr = [];
                for (var i = 0; i < str.length; i += nChar) {
                    arr.push(str.slice(i, i + nChar));
                }
                return arr;
            }

            blocks.forEach((item, index) => {
                addNewQrCode(index, item, true);
                var qrcode = new QRCode("qrcode" + index, {
                    correctLevel : QRCode.CorrectLevel.L
                });
                qrcode.makeCode(item);
            });

            // global hash at bottom
            var globalHash2 = document.createElement('p');
            globalHash2.setAttribute("class", "h4");
            globalHash2.innerText = 'Empreinte SHA-1 globale (textuelle et visuelle) : ' + separatorEveryNChar(hash, ' ', 4);;
            qrcodeWrapper.appendChild(globalHash2);

            // global visual hash at bottom
            var globalVisualHashCanvas2 = document.createElement('canvas');
            globalVisualHashCanvas2.setAttribute("id", "globalVisualHashCanvas2");
            globalVisualHashCanvas2.setAttribute("class", "qrcode__pad");
            globalVisualHashCanvas2.setAttribute("title", "Empreinte visuelle globale SHA-1");
            qrcodeWrapper.appendChild(globalVisualHashCanvas2);

            var canvas2 = document.getElementById("globalVisualHashCanvas2");
            visualHash(canvas2, hash, 256, 256);
        } 
        else {
            // One QR Code
            var numberOfQRCodes = document.createElement('p');
            numberOfQRCodes.innerText = 'Un seul QR Code a été généré pour transporter ce contenu textuel.';
            qrcodeWrapper.appendChild(numberOfQRCodes);

            addNewQrCode(0, variousContent, false);
            var qrcode = new QRCode("qrcode", {
                correctLevel : QRCode.CorrectLevel.L
            });
            qrcode.makeCode(variousContent);
        }
    }

    // generate one or many QR Codes
    var generateQRCode = document.getElementById('generateQRCode');
    if(generateQRCode){
        generateQRCode.addEventListener('click', function(e){
            e.preventDefault();
            var errorQRCode = document.getElementById("errorQRCode");
            errorQRCode.innerHTML = '';

            const myNode = document.getElementById("qrcodeWrapper");
                  myNode.innerHTML = '';
            qrcodeWrapper.style.display = 'block';
            setDisabled('generateQRCode');
            makeCode();
        })
    }

    // cancel
    var cancelQRCode = document.getElementById('cancelQRCode');
    if(cancelQRCode){
        cancelQRCode.addEventListener('click', function(e){
            e.preventDefault();
            var errorQRCode = document.getElementById("errorQRCode");
            errorQRCode.innerHTML = '';

            var variousContent = document.getElementById("variousContent");
                  variousContent.value = '';

            var nodeQrcode = document.getElementById("qrcode");
                  nodeQrcode.innerHTML = '';

            var nodeQrcodeWrapper = document.getElementById("qrcodeWrapper");
                  nodeQrcodeWrapper.innerHTML = '';

            qrcodeWrapper.style.display = 'block';

            var displayEnteredCharQRC = document.getElementById("displayEnteredCharQRC");  
            displayEnteredCharQRC.textContent = 0;

            removeDisabled('generateQRCode');
        })
    }
})(visualHash);


// *******
// RECEIVE
// *******
(function() {

    "use strict";
    
    setDisabled('cancelNumberOfQRCodesToRead');
    
    // generate X textarea to receive content of each QR Code
    document.getElementById("btnNumberOfQRCodesToRead").addEventListener('click', function(e) {
        var numberOfQRCodes = document.getElementById("numberOfQRCodesToRead").value;  
        var errorNumberOfQRCodesToRead = document.getElementById("errorNumberOfQRCodesToRead");  

        if(numberOfQRCodes == '' || !isNumber(numberOfQRCodes)) {
            errorNumberOfQRCodesToRead.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.numberOfQRCodesToReadEmpty') + '</p>'
            return;
        }
        createQRCodes(numberOfQRCodes);
        setDisabled('numberOfQRCodesToRead');
        removeDisabled('cancelNumberOfQRCodesToRead');
    });

    // cancel
    document.getElementById("cancelNumberOfQRCodesToRead").addEventListener('click', function(e) {
        var numberOfQRCodes = document.getElementById("numberOfQRCodesToRead");
        numberOfQRCodes.value = '';
        errorNumberOfQRCodesToRead.innerHTML = '';
        QRCReceiverWrapper.innerHTML = '';

        setDisabled('cancelNumberOfQRCodesToRead');
        removeDisabled('numberOfQRCodesToRead');      
        removeDisabled('btnNumberOfQRCodesToRead');    
    });

    // multiples QR Codes
    function createQRCodes(nb) {

        var QRCReceiverWrapper = document.getElementById('QRCReceiverWrapper');

        for (var i = 0; i < nb; i++) {
            var newDivTitle = document.createElement('p');
            var newDivHash = document.createElement('p');
            var visualHashCanvas = document.createElement('canvas');
            var newTextarea = document.createElement('textarea');
            var newError = document.createElement('p');
            var newButton = document.createElement('button');
            var newCancelButton = document.createElement('button');
            var newHr = document.createElement('br');

            newError.setAttribute("id", "errorHashQRC" + (i + 1));
            
            newButton.innerText = 'Vérifier l\'empreinte';
            newButton.setAttribute("id", "buttonQRC" + (i + 1));
            newButton.setAttribute("class", "btn btn-sm btn-warning btn__qrc--receiver");
            
            newCancelButton.innerText = 'Annuler';
            newCancelButton.setAttribute("id", "cancelButtonQRC" + (i + 1));
            newCancelButton.setAttribute("class", "btn btn-sm btn-outline-danger btn__qrc--receiver");
            
            newDivHash.setAttribute("id", "hashQRC" + (i + 1));
            
            newDivTitle.innerText = 'QR Code n°' + (i + 1);
            newDivTitle.setAttribute("class", "h3");

            // visual hash
            visualHashCanvas.setAttribute("id", "receiverHashCanvas" + (i + 1));
            visualHashCanvas.setAttribute("class", "mg5");
            visualHashCanvas.setAttribute("title", "Empreinte visuelle SHA-1");

            newTextarea.setAttribute("id", "QRC" + (i + 1));
            newTextarea.setAttribute("class", "form-control");

            QRCReceiverWrapper.appendChild(newDivTitle);
            QRCReceiverWrapper.appendChild(newDivHash);
            QRCReceiverWrapper.appendChild(visualHashCanvas);
            QRCReceiverWrapper.appendChild(newTextarea);
            QRCReceiverWrapper.appendChild(newError);
            QRCReceiverWrapper.appendChild(newButton);
            QRCReceiverWrapper.appendChild(newCancelButton);
            QRCReceiverWrapper.appendChild(newHr);

            // display none visual hash
            visualHashCanvas.style.display = 'none';
            newError.style.display = 'none';

            document.getElementById("buttonQRC" + (i + 1))
            .addEventListener('click', function(e) {
                var textarea = this.previousElementSibling.previousElementSibling;
                var errorHash = this.previousElementSibling;
                errorHash.innerHTML = '';
                errorHash.style.display = 'none';

                if(textarea.value == '') {
                    errorHash.style.display = 'block';
                    errorHash.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.QRCodeEmpty') + '</p>'
                    return;
                }

                var hashWrapper = this.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling;
                // console.log('prevprevprevprev : ', hashWrapper);

                // textual hash
                var hash = CryptoJS.SHA1(textarea.value.trim()).toString().toUpperCase();
                hashWrapper.innerText = separatorEveryNChar(hash, ' ', 4) + ' (empreinte SHA-1)';
                
                // visual hash
                var hashVisualCanvas = this.previousElementSibling.previousElementSibling.previousElementSibling;
                visualHash(hashVisualCanvas, hash);
                hashVisualCanvas.style.display = 'block';
            });

            document.getElementById("cancelButtonQRC" + (i + 1))
            .addEventListener('click', function(e) {
                var hashVisualCanvas = this.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling;
                var textarea = this.previousElementSibling.previousElementSibling.previousElementSibling;
                var errorHash = this.previousElementSibling.previousElementSibling;
                var hashWrapper = this.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling;
                
                hashVisualCanvas.style.display = 'none';
                errorHash.style.display = 'none';
                newError.style.display = 'none';
                hashWrapper.innerText = '';
                textarea.value = '';
            });
        }

        // validate global hash of all QR Codes
        // ------------------------------------
        if(nb > 1) { // display this global hash check only if more than one QR Code.
            var newValidateAll = document.createElement('button');
            var newErrorValidateAllHash = document.createElement('p');
            var newValidateAllHash = document.createElement('p');
            var newValidateAllVisualHash = document.createElement('canvas');
            var labelConcatenatedContent = document.createElement('label');
            var concatenatedContent = document.createElement('textarea');
            var copyConcatenatedContent = document.createElement('button');

            newValidateAllVisualHash.setAttribute("class", "mg5");
            newValidateAllVisualHash.setAttribute("title", 'Empreinte visuelle globale SHA-1');
            newValidateAllVisualHash.setAttribute("id", 'newValidateAllVisualHash');
            newValidateAllVisualHash.setAttribute("id", 'newValidateAllVisualHash');

            newValidateAll.setAttribute("class", "btn btn-primary btn__qrc--receiver");
            newValidateAll.setAttribute("id", "btnCheckAllQRCodesToRead");
            newValidateAll.innerText = 'RECONSTITUER ET VERIFIER GLOBALEMENT';
            newErrorValidateAllHash.setAttribute("id", "newErrorValidateAllHash");
            
            labelConcatenatedContent.setAttribute("class", "control-label");
            labelConcatenatedContent.innerText = 'Reconstitution du contenu transféré par QR Codes';
            concatenatedContent.setAttribute("class", "form-control");
            concatenatedContent.setAttribute("rows", "10");
            concatenatedContent.setAttribute("id", "concatenatedContent");
            
            // copyConcatenatedContent.innerText = 'Copier';
            // copyConcatenatedContent.setAttribute("id", "copyConcatenatedContent");
            // copyConcatenatedContent.setAttribute("class", "btn btn-sm btn-outline-primary btn__copy");
            // copyConcatenatedContent.setAttribute("data-clipboard-target", "concatenatedContent");

            QRCReceiverWrapper.appendChild(newValidateAll);
            QRCReceiverWrapper.appendChild(newErrorValidateAllHash);
            QRCReceiverWrapper.appendChild(newValidateAllHash);
            QRCReceiverWrapper.appendChild(newValidateAllVisualHash);
            QRCReceiverWrapper.appendChild(labelConcatenatedContent);
            QRCReceiverWrapper.appendChild(concatenatedContent);
            // QRCReceiverWrapper.appendChild(copyConcatenatedContent);

            setDisabled('btnNumberOfQRCodesToRead');
            newValidateAllVisualHash.style.display = 'none';
            labelConcatenatedContent.style.display = 'none';
            concatenatedContent.style.display = 'none';
            // copyConcatenatedContent.style.display = 'none';

            // validate at the end to display full hash of concatenated QR Codes
            document.getElementById("btnCheckAllQRCodesToRead").addEventListener('click', function(e) {
                var newErrorValidateAllHash = document.getElementById("newErrorValidateAllHash")
                newErrorValidateAllHash.innerHTML = '';

                // QR Codes empty
                if(concatenatedQRCodesHash() === false) {
                    const context = newValidateAllVisualHash.getContext('2d');
                    context.clearRect(0, 0, newValidateAllVisualHash.width, newValidateAllVisualHash.height);
                    newValidateAllHash.innerText = '';
                    newErrorValidateAllHash.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.QRCodesEmpty') + '</p>'
                    return;
                }

                // textual hash
                newValidateAllHash.innerText = 'Empreinte textuelle et visuelle SHA-1 de vérification finale de tous les contenus de chaque QR Code ci-dessus réunis : ' + separatorEveryNChar(concatenatedQRCodesHash(), ' ', 4);

                // visual global hash
                visualHash(newValidateAllVisualHash, concatenatedQRCodesHash(), 256, 256);
                ; 

                // insert concatenated content in textarea
                 var concatenatedQRCodes = '';
                for (var i = 0; i < nb; i++) {
                    concatenatedQRCodes += document.getElementById("QRC" + (i + 1)).value.trim();
                }

                // display visual hash and concatenated textarea and feed it
                newValidateAllVisualHash.style.display = 'block';
                labelConcatenatedContent.style.display = 'block';
                concatenatedContent.style.display = 'block';
                concatenatedContent.value = concatenatedQRCodes;
                // copyConcatenatedContent.style.display = 'block';

                // const buttonCopy = document.getElementById('copyConcatenatedContent');
                // buttonCopy.onclick = function() {
                //     const target = buttonCopy.previousElementSibling;
                //     // Init clipboard -- offical documentation: https://clipboardjs.com/
                //     var clipboard = new ClipboardJS(buttonCopy, {
                //         target: target,
                //         text: function() {
                //             return target.value;
                //         }
                //     });

                //     // Success action handler
                //     clipboard.on('success', function(e) {
                //         const currentLabel = buttonCopy.innerHTML;

                //         // Exit label update when already in progress
                //         if(buttonCopy.innerHTML === 'Copié !'){
                //             return;
                //         }
                //         // Update button label
                //         buttonCopy.innerHTML = 'Copié !';

                //         // Revert button label after 3 seconds
                //         setTimeout(function(){
                //             buttonCopy.innerHTML = currentLabel;
                //         }, 3000)
                //     });
                // }

            });

            // concatenate all QR Codes and calculate hash
            function concatenatedQRCodesHash() {
                var noEmptyBlock = true;
                var concatenatedQRCodes = '';
                for (var i = 0; i < nb; i++) {
                    if(document.getElementById("QRC" + (i + 1)).value == false) {
                        noEmptyBlock = false;
                    }
                    concatenatedQRCodes += document.getElementById("QRC" + (i + 1)).value.trim();
                }
                var concatenatedQRCodesHash = CryptoJS.SHA1(concatenatedQRCodes).toString().toUpperCase();

                if (noEmptyBlock === true) {
                    return concatenatedQRCodesHash;
                }
                return false;
            }
        }
    }
})(visualHash);