// *****
// i18n
// *****

const i18nElements = $("[data-i18n]");

i18next.init({
    lng: 'fr',
    fallbackLng: 'fr',
    interpolation: {
        escapeValue: false,
    },
    debug: true,
    resources
}, function(err, t) {
    updateContent();
    if (err) return console.error(err);
});

function updateContent() {
    i18nElements.map(function (i, el) {
        const key = $(el).data('i18n');
        el.innerHTML = i18next.t(key);
    });
}

function changeLng(event, lng) {
    event.preventDefault();
    i18next.changeLanguage(lng);
}

i18next.on('languageChanged', () => {
    updateContent();
});
