// *****************
// OTP NUMERIC ONLY
// *****************

// ***********************
// CONVERT - OTP NUMERIC
// ***********************

// ****************************************
// CONVERT FROM CLEAR MESSAGE TO CLEAR CODE
// ****************************************
(function() {

    // "use strict";

    function convertFromTextToCode(str) {
        var chain = '';
        var temp = '';
        var j = 0;
        for (var i=0; i < str.length; i++) {
            switch (str[i]) {
                case "A":
                    temp = 1;
                    chain += temp;
                    break;

                case "E":
                    temp = 2;
                    chain += temp;
                    break;

                case "I":
                    temp = 3;
                    chain += temp;
                    break;

                case "N":
                    temp = 4;
                    chain += temp;
                    break;

                case "S":
                    temp = 5;
                    chain += temp;
                    break;
                
                case "T":
                    temp = 6;
                    chain += temp;
                    break;

                case "B":
                    temp = 70;
                    chain += temp;
                    break;

                case "C":
                    temp = 71;
                    chain += temp;
                    break;

                case "D":
                    temp = 72;
                    chain += temp;
                    break;

                case "F":
                    temp = 73;
                    chain += temp;
                    break;

                case "G":
                    temp = 74;
                    chain += temp;
                    break;

                case "H":
                    temp = 75;
                    chain += temp;
                    break;

                case "J":
                    temp = 76;
                    chain += temp;
                    break;

                case "K":
                    temp = 77;
                    chain += temp;
                    break;

                case "L":
                    temp = 78;
                    chain += temp;
                    break;

                case "M":
                    temp = 79;
                    chain += temp;
                    break;

                case "O":
                    temp = 80;
                    chain += temp;
                    break;

                case "P":
                    temp = 81;
                    chain += temp;
                    break;

                case "Q":
                    temp = 82;
                    chain += temp;
                    break;

                case "R":
                    temp = 83;
                    chain += temp;
                    break;

                case "U":
                    temp = 84;
                    chain += temp;
                    break;

                case "V":
                    temp = 85;
                    chain += temp;
                    break;

                case "W":
                    temp = 86;
                    chain += temp;
                    break;

                case "X":
                    temp = 87;
                    chain += temp;
                    break;                    

                case "Y":
                    temp = 88;
                    chain += temp;
                    break;

                case "Z":
                    temp = 89;
                    chain += temp;
                    break;

                case "[":
                    if(j == 0) {
                        j = 1;
                    } else {
                        j = 0;
                    }
                    temp = 90;
                    chain += temp;
                    i = i+1;
                    break;

                case ".":
                    temp = 91;
                    chain += temp;
                    break;

                case ":":
                    temp = 92;
                    chain += temp;
                    break;

                case "'":
                    temp = 93;
                    chain += temp;
                    break;

                case "(":
                    i = i+1;
                    temp = 94;
                    chain += temp;
                    break;

                case "+":
                    temp = 95;
                    chain += temp;
                    break;

                case "-":
                    temp = 96;
                    chain += temp;
                    break;

                case "=":
                    temp = 97;
                    chain += temp;
                    break;

                case "?":
                    temp = 98;
                    chain += temp;
                    break;

                case " ":
                    if(j == 1) {
                        return(i18next.t('error.bracketsMissingForSpace'));
                    } else {
                        if(str[i + 1] == ' ') {
                            var test = 'tester';
                            return(i18next.t('error.doubleSpaces') + str[i - 1] + ' à la '+ i + 'e position.');
                        }
                        temp = 99;
                        chain += temp;
                        break;
                    }

                case '0':
                    if(j == 1) {
                        temp = '000';
                        chain += temp;
                        break;
                    } else {
                        if ( !isAlphaUpper(str[i + 3]) && !isAlphaUpper(str[i + 2]) && !isAlphaUpper(str[i + 1]) ) {
                            temp = '0' + str[i+1] + str[i+2] + str[i+3];
                            chain += temp;
                            i = i+3;
                            break;
                        } 
                        else{
                            return(i18next.t('error.bracketsMissing'));
                        }
                    }

                case '1':
                    if(j == 1) {
                        temp = '111';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '2':
                    if(j == 1) {
                        temp = '222';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '3':
                    if(j == 1) {
                        temp = '333';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '4':
                    if(j == 1) {
                        temp = '444';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '5':
                    if(j == 1) {
                        temp = '555';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '6':
                    if(j == 1) {
                        temp = '666';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '7':
                    if(j == 1) {
                        temp = '777';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '8':
                    if(j == 1) {
                        temp = '888';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                case '9':
                    if(j == 1) {
                        temp = '999';
                        chain += temp;
                        break;
                    } else {
                        return(i18next.t('error.bracketsMissing'));
                    }

                default:
                    console.log("Désolé, nous n'avons pas de correspondance.");
            }
        }
        return chain;
    }

    // convert
    var btnOTPNumeric = document.getElementById('btnOTPNumeric');
    if(btnOTPNumeric){
        btnOTPNumeric.addEventListener('click', function(e){
            e.preventDefault();

            // remove potential error displayed
            var errorOTPNumeric = document.getElementById("errorOTPNumeric");
            errorOTPNumeric.innerHTML = '';
            
            var OTPTextToNumeric = document.getElementById('OTPTextToNumeric').value;
            var outputOTPTextToNumeric = document.getElementById('outputOTPTextToNumeric');
            
            if('' == OTPTextToNumeric ) {                           
                document.getElementById("errorOTPNumeric").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer un contenu textuel en clair à convertir.</p>';
                return;
            }

            if(!isAlphaNumUpperOTPNumeric(OTPTextToNumeric)) {                                                   
                document.getElementById("errorOTPNumeric").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPUpperOnlyEmpty') + '</p>'
                return;
            }

            if(!checkModuloBrackets('OTPTextToNumeric')) {                                                   
                document.getElementById("errorOTPNumeric").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.moduloBrackets') + '</p>'
                return;
            }

            if(!checkModuloParenthesis('OTPTextToNumeric')) {                                                   
                document.getElementById("errorOTPNumeric").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.moduloParenthesis') + '</p>'
                return;
            }
            // separate by space each five caracters block and return
            outputOTPTextToNumeric.value = OTPSeparatorEveryNChar(convertFromTextToCode(OTPTextToNumeric), ' ');
            setDisabled('OTPTextToNumeric');
            removeDisabled('outputOTPTextToNumeric');
        });

        // cancel
        var cancelOTPNumeric = document.getElementById('cancelOTPNumeric');
        if(cancelOTPNumeric){
            cancelOTPNumeric.addEventListener('click', function(e){
                e.preventDefault();
                var errorOTPNumeric = document.getElementById("errorOTPNumeric");
                errorOTPNumeric.innerHTML = '';

                const OTPTextToNumeric = document.getElementById("OTPTextToNumeric");
                const outputOTPTextToNumeric = document.getElementById("outputOTPTextToNumeric");
                OTPTextToNumeric.value = '';
                outputOTPTextToNumeric.value = '';

                removeDisabled('OTPTextToNumeric');
                setDisabled('outputOTPTextToNumeric');
            })
        }

        // check brackets [] are even
        function checkModuloBrackets(id) {
            var textareaValue = document.getElementById(id).value;
            var regex = /[[]/g; // count occurences of opening bracket ([)
            if((textareaValue.match(regex)) != null) {
                var countBrackets = textareaValue.match(regex).length;
                if(countBrackets % 2 === 0) {
                    return true;
                } 
                return false;
            } 
            return true;
        }

        // check parenthesis () are even
        function checkModuloParenthesis(id) {
            var textareaValue = document.getElementById(id).value;
            var regex = /[(]/g; // count occurences of opening parenthesis (()
            if((textareaValue.match(regex)) != null) {
                var countParenthesis = textareaValue.match(regex).length;
                if(countParenthesis % 2 === 0) {
                    return true;
                } else {
                    return false;
                }
            } 
            return true;
        }
    }
})();


// ****************************************
// CONVERT FROM CLEAR CODE TO CLEAR MESSAGE
// ****************************************

(function() {

    // "use strict";

    // convert with french table code
    function convertFromCodeToText(str) {
        var chain = '';
        var item = '';
        var temp1 = '';
        var j = 0;
        for (var i=0; i < str.length; i++) {
            switch (str[i]) {
                case '0':
                    if(j == 1) {
                        temp = '000';
                        chain += temp;
                        break;
                    } else {
                        if ( !isAlphaUpper(str[i + 3]) && !isAlphaUpper(str[i + 2]) && !isAlphaUpper(str[i + 1]) ) {
                            temp = '0' + str[i+1] + str[i+2] + str[i+3];
                            chain += temp;
                            i = i+3;
                            break;
                        } 
                        else{
                            return(i18next.t('error.bracketsMissing'));
                        }
                    }

                case "1":
                    if(j == 1) {
                        chain += '1';
                        i = i+2;
                        break;
                    }
                    item = 'A';
                    chain += item;
                    break;

                case "2":
                    if(j == 1) {
                        chain += '2';
                        i = i+2;
                        break;
                    }
                    item = 'E';
                    chain += item;
                    break;

                case "3":
                    if(j == 1) {
                        chain += '3';
                        i = i+2;
                        break;
                    }
                    item = 'I';
                    chain += item;
                    break;

                case "4":
                    if(j == 1) {
                        chain += '4';
                        i = i+2;
                        break;
                    }
                    item = 'N';
                    chain += item;
                    break;

                case "5":
                    if(j == 1) {
                        chain += '5';
                        i = i+2;
                        break;
                    }
                    item = 'S';
                    chain += item;
                    break;
                
                case "6":
                    if(j == 1) {
                        chain += '6';
                        i = i+2;
                        break;
                    }
                    item = 'T';
                    chain += item;
                    break;

                case "7":
                    var temp1 = str[i + 1];
                    if(temp1 == 0) {
                        chain += 'B';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 1) {
                        chain += 'C';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 2) {
                        chain += 'D';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 3) {
                        chain += 'F';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 4) {
                        chain += 'G';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 5) {
                        chain += 'H';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 6) {
                        chain += 'J';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 7) {
                        if(j == 1) {
                            chain += '7';
                            i = i+2;
                            break;
                        }
                        chain += 'K';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 8) {
                        chain += 'L';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 9) {
                        chain += 'M';
                        i = i+1;
                        break;
                    }

                case "8":
                    var temp1 = str[i + 1];
                    if(temp1 == 0) {
                        chain += 'O';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 1) {
                        chain += 'P';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 2) {
                        chain += 'Q';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 3) {
                        chain += 'R';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 4) {
                        chain += 'U';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 5) {
                        chain += 'V';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 6) {
                        chain += 'W';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 7) {
                        chain += 'X';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 8) {
                        if(j == 1) {
                            chain += '8';
                            i = i+2;
                            break;
                        }
                        chain += 'Y';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 9) {
                        chain += 'Z';
                        i = i+1;
                        break;
                    }

                case "9":
                    var temp1 = str[i + 1];
                    if(temp1 == 0) {
                        chain += '[]';
                        i = i+1;
                        if(j == 0) {
                            j = 1;
                        } else {
                            j = 0;
                        }
                        break;
                    }
                    else if(temp1 == 1) {
                        chain += '.';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 2) {
                        chain += ':';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 3) {
                        chain += "'";
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 4) {
                        chain += '()';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 5) {
                        chain += '+';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 6) {
                        chain += '-';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 7) {
                        chain += '=';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 8) {
                        chain += '?';
                        i = i+1;
                        break;
                    }
                    else if(temp1 == 9) {
                        if(j == 1) {
                            chain += '9';
                            i = i+2;
                            break;
                        }
                        chain += ' ';
                        i = i+1;
                        break;
                    } 
                    else { // a 9 alone should not happen, just sometimes at the end if no place for 91 to end block of five.
                        chain += 'Ø';
                        i = i+1;
                        break;
                    }

                default:
                    console.log("Nous ne trouvons pas de correspondance pour ce caractère.");
            }

        }
        return chain;
    }
    
    // convert clear code to clear message
    var btnDecOTPNumeric = document.getElementById('btnDecOTPNumeric');
    if(btnDecOTPNumeric){
        btnDecOTPNumeric.addEventListener('click', function(e){
            e.preventDefault();
            // remove potentiel error displayed
            var errorDecOTPNumeric = document.getElementById("errorDecOTPNumeric");
            errorDecOTPNumeric.innerHTML = '';

            var decOTPTextToNumeric = removeSpaces(document.getElementById('decOTPTextToNumeric').value);
            var decOutputOTPTextToNumeric = document.getElementById('decOutputOTPTextToNumeric');

            if('' == decOTPTextToNumeric ) {                           
                errorDecOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer un code en clair à convertir.</p>';
                return;
            }

            if(!isNumber(decOTPTextToNumeric)) {                                                   
                errorDecOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.clearCodeNotNumber') + '</p>'
                return;
            }
            // finally convert to clear text
            decOutputOTPTextToNumeric.value = convertFromCodeToText(decOTPTextToNumeric);
            setDisabled('decOTPTextToNumeric');
            removeDisabled('decOutputOTPTextToNumeric');
        });

        // cancel
        var cancelDecOTPNumeric = document.getElementById('cancelDecOTPNumeric');
        if(cancelDecOTPNumeric){
            cancelDecOTPNumeric.addEventListener('click', function(e){
                e.preventDefault();
                var errorDecOTPNumeric = document.getElementById("errorDecOTPNumeric");
                errorDecOTPNumeric.innerHTML = '';

                var decOTPTextToNumeric = document.getElementById("decOTPTextToNumeric");
                var decOutputOTPTextToNumeric = document.getElementById("decOutputOTPTextToNumeric");
                decOTPTextToNumeric.value = '';
                decOutputOTPTextToNumeric.value = '';

                removeDisabled('decOTPTextToNumeric');
                setDisabled('decOutputOTPTextToNumeric');
            })
        }

        // button delete brackets []
        var btnDeleteBracketsOTPNumeric = document.getElementById('btnDeleteBracketsOTPNumeric');
        if(btnDeleteBracketsOTPNumeric){
            btnDeleteBracketsOTPNumeric.addEventListener('click', function(e){
                e.preventDefault();

                var decOutputOTPTextToNumeric = document.getElementById("decOutputOTPTextToNumeric");
                var toRemove = decOutputOTPTextToNumeric.value;
                decOutputOTPTextToNumeric.value = removeBrackets(toRemove);
                // setDisabled('btnDeleteBracketsOTPNumeric');
            })
        }
    }
})();


// *********************************
// CRYPT CLEAR CODE WITH OTP NUMERIC
// *********************************

(function() {

    // "use strict";
    function cryptOTPNumeric(clearCode, OTPNumeric) {
        var chain = '';
        var temp = '';
        for (var i=0; i < clearCode.length; i++) {
            if(clearCode[i] < OTPNumeric[i]) {
                temp = 1 + clearCode[i] - OTPNumeric[i];
            } else {
                temp = clearCode[i] - OTPNumeric[i];
            }
            chain += temp;
        }
        return separatorOTPNumericEveryNChar(chain, ' ', 5);
    }
    
    var btnCipherOTPNumeric = document.getElementById('btnCipherOTPNumeric');
    if(btnCipherOTPNumeric){
        btnCipherOTPNumeric.addEventListener('click', function(e){
            e.preventDefault();
            var errorCipherOTPNumeric = document.getElementById("errorCipherOTPNumeric");
            errorCipherOTPNumeric.innerHTML = '';

            var OTPClearCode = document.getElementById('OTPClearCode').value;
            if('' == OTPClearCode) {                           
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.clearCodeEmpty') + '</p>'
                return;
            }

            if(!isNumber(OTPClearCode.replace(/\s/g,''))) {                           
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.clearCodeNotNumber') + '</p>'
                return;
            }

            // add . (9191) to complete last block if needed
            // and adjust counter of letters
            completeOTPNumericBlockOf5('OTPClearCode');

            var OTPNumeric = document.getElementById('OTPNumeric').value;
            if('' == OTPNumeric) {                           
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPNumericEmpty') + '</p>'
                return;
            }

            if(!checkMultiple5Block('OTPNumeric')) {                           
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPNumericNotMultiple5Block') + '</p>'
                return;
            }

            // recalculate again the value of both textarea as they may have changed especially with completeOTPNumericBlockOf5('OTPClearCode');
            var OTPNumeric = document.getElementById('OTPNumeric').value;
            var OTPClearCode = document.getElementById('OTPClearCode').value;
            if(OTPClearCode.replace(/\s/g,'') == OTPNumeric.replace(/\s/g,'')) {
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.clearCodeAndOTPIdentical') + '</p>'
                return;
            }
                        
            if(OTPClearCode.replace(/\s/g,'').length != OTPNumeric.replace(/\s/g,'').length) {                           
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPNumericLongEnough') + '</p>'
                return;
            } 

            if(!isNumber(OTPNumeric.replace(/\s/g,''))) {                           
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPNumericNotNumber') + '</p>'
                return;
            }

            try {
                cipheredTextByOTPNumeric.value = cryptOTPNumeric(OTPClearCode.replace(/\s/g,''), OTPNumeric.replace(/\s/g,''));
                setDisabled('btnCipherOTPNumeric');
                setDisabled('OTPClearCode');
                setDisabled('OTPNumeric');
                removeDisabled('cipheredTextByOTPNumeric');
         
            } 
            catch (e) {
                errorCipherOTPNumeric.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Erreur One-Time-Pad : ' + e.message + '.</p>'
                return;
            }
        });

        // cancel crypt
        var cancelCipherOTPNumeric = document.getElementById('cancelCipherOTPNumeric');
        if(cancelCipherOTPNumeric){
            cancelCipherOTPNumeric.addEventListener('click', function(e){
                e.preventDefault();
                var errorCipherOTPNumeric = document.getElementById("errorCipherOTPNumeric");
                errorCipherOTPNumeric.innerHTML = '';

                const OTPClearCode = document.getElementById("OTPClearCode");
                const OTPNumeric = document.getElementById("OTPNumeric");
                const cipheredTextByOTPNumeric = document.getElementById("cipheredTextByOTPNumeric");
                OTPClearCode.value = '';
                OTPNumeric.value = '';
                cipheredTextByOTPNumeric.value = '';

                // reset char. counters
                var charCounterCryptClearCode = document.getElementById('charCounterCryptClearCode');
                charCounterCryptClearCode.textContent = 0;

                var charCounterCryptOTPNumeric = document.getElementById('charCounterCryptOTPNumeric');
                charCounterCryptOTPNumeric.textContent = 0;

                removeDisabled('btnCipherOTPNumeric');
                removeDisabled('OTPClearCode');
                removeDisabled('OTPNumeric');
                removeDisabled('btnSuppressSpacesOTPNumeric');

                setDisabled('cipheredTextByOTPNumeric');
            })
        }
        
        // delete spaces
        var btnSuppressSpacesOTPNumeric = document.getElementById('btnSuppressSpacesOTPNumeric');
        if(btnSuppressSpacesOTPNumeric){
            btnSuppressSpacesOTPNumeric.addEventListener('click', function(e){
                e.preventDefault();
                var errorCipherOTPNumeric = document.getElementById("errorDecOTPNumeric");
                errorCipherOTPNumeric.innerHTML = '';

                var cipheredTextByOTPNumeric = document.getElementById("cipheredTextByOTPNumeric");
                if('' == cipheredTextByOTPNumeric.value ) {                           
                    document.getElementById("errorDecOTPNumeric").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Aucun résultat chiffré pour en supprimer les espaces.</p>';
                    return;
                }
                var cipheredContent = cipheredTextByOTPNumeric.value;
                cipheredTextByOTPNumeric.value = removeSpaces(cipheredContent);
                // cipheredTextByOTPNumeric.value = '';

                setDisabled('btnSuppressSpacesOTPNumeric');
            })
        }
    }

    // CRYPT - COUNT CHARACTERS - OTP NUMERIC

    // Clear Code
    var charCounterCryptClearCode = document.getElementById("charCounterCryptClearCode");   
    var OTPClearCodeTextarea = document.querySelector('#OTPClearCode');
    const countCharactersClearCode = () => {
        let numOfEnteredChars = OTPClearCodeTextarea.value.replace(/\s/g,'').length;
        charCounterCryptClearCode.textContent = numOfEnteredChars;
    };
    OTPClearCodeTextarea.addEventListener("input", countCharactersClearCode);

    // Numeric key (OTP)
    var charCounterCryptOTPNumeric = document.getElementById("charCounterCryptOTPNumeric");   
    var OTPNumericTextarea = document.querySelector('#OTPNumeric');
    const countCharactersOTPNumeric = () => {
        let numOfEnteredChars = OTPNumericTextarea.value.replace(/\s/g,'').length;
        charCounterCryptOTPNumeric.textContent = numOfEnteredChars;
    };
    OTPNumericTextarea.addEventListener("input", countCharactersOTPNumeric);
})();



// ***********************************
// DECRYPT CLEAR CODE WITH OTP NUMERIC
// ***********************************

(function() {

    // "use strict";

    function decryptOTPNumeric(OTPEncryptedCodeTextarea, OTPNumericDecryptTextarea) {
        var chain = '';
        var temp = '';
        var result = '';
        for (var i=0; i < OTPEncryptedCodeTextarea.length; i++) {
            result = parseFloat(OTPEncryptedCodeTextarea[i]) + parseFloat(OTPNumericDecryptTextarea[i]);
            console.log(result);
            if(parseFloat(result) >= 10) {
                temp = parseFloat(result) - 10;
            } else {
                temp = (result);
            }
            chain += temp;
        }
        return separatorOTPNumericEveryNChar(chain, ' ', 5);
    }
    
    var btnDecipherOTPNumeric = document.getElementById('btnDecipherOTPNumeric');

    if(btnDecipherOTPNumeric){
        btnDecipherOTPNumeric.addEventListener('click', function(e){
            e.preventDefault();
           
            var errorOTPNumericDecrypt = document.getElementById("errorOTPNumericDecrypt");
            errorOTPNumericDecrypt.innerHTML = '';

            var OTPEncryptedCodeTextarea = document.getElementById('OTPEncryptedCodeTextarea').value;
            if('' == OTPEncryptedCodeTextarea) {                           
                errorOTPNumericDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.encryptedCodeEmpty') + '</p>'
                return;
            }

            if(!isNumber(OTPEncryptedCodeTextarea.replace(/\s/g,''))) {                           
                errorOTPNumericDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.encryptedCodeNotNumber') + '</p>'
                return;
            }

            var OTPNumericDecryptTextarea = document.getElementById('OTPNumericDecryptTextarea').value;
            if('' == OTPNumericDecryptTextarea) {                           
                errorOTPNumericDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPNumericEmpty') + '</p>'
                return;
            }

            if(OTPEncryptedCodeTextarea.replace(/\s/g,'') == OTPNumericDecryptTextarea.replace(/\s/g,'')) {
                errorOTPNumericDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.encryptedCodeAndOTPIdentical') + '</p>'
                return;
            }

            if(OTPEncryptedCodeTextarea.replace(/\s/g,'').length != OTPNumericDecryptTextarea.replace(/\s/g,'').length) {                           
                errorOTPNumericDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPNumericEncryptedCodeLongEnough') + '</p>'
                return;
            } 

            if(!isNumber(OTPNumericDecryptTextarea.replace(/\s/g,''))) {                           
                errorOTPNumericDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPNumericNotNumber') + '</p>'
                return;
            }

            decipheredClearCodeByOTPNumeric.value = decryptOTPNumeric(OTPEncryptedCodeTextarea.replace(/\s/g,''), OTPNumericDecryptTextarea.replace(/\s/g,''));

            setDisabled('btnDecipherOTPNumeric');
            setDisabled('OTPEncryptedCodeTextarea');
            setDisabled('OTPNumericDecryptTextarea');
            removeDisabled('decipheredClearCodeByOTPNumeric');
        });

        // cancel decrypt
        var cancelDecipherOTPNumeric = document.getElementById('cancelDecipherOTPNumeric');
        if(cancelDecipherOTPNumeric){
            cancelDecipherOTPNumeric.addEventListener('click', function(e){
                e.preventDefault();
                var errorOTPNumericDecrypt = document.getElementById("errorOTPNumericDecrypt");
                errorOTPNumericDecrypt.innerHTML = '';

                var OTPEncryptedCodeTextarea = document.getElementById("OTPEncryptedCodeTextarea");
                var OTPNumericDecryptTextarea = document.getElementById("OTPNumericDecryptTextarea");
                var decipheredClearCodeByOTPNumeric = document.getElementById("decipheredClearCodeByOTPNumeric");
                OTPEncryptedCodeTextarea.value = '';
                OTPNumericDecryptTextarea.value = '';
                decipheredClearCodeByOTPNumeric.value = '';

                // reset char. counters
                var charCounterEncryptedCode = document.getElementById('charCounterEncryptedCode');
                charCounterEncryptedCode.textContent = 0;

                var charCounterOTPNumericDecrypt = document.getElementById('charCounterOTPNumericDecrypt');
                charCounterOTPNumericDecrypt.textContent = 0;

                removeDisabled('btnDecipherOTPNumeric');
                removeDisabled('OTPEncryptedCodeTextarea');
                removeDisabled('OTPNumericDecryptTextarea');
                setDisabled('decipheredClearCodeByOTPNumeric');
            })
        }
    }

    // DECRYPT - COUNT CHARACTERS - OTP NUMERIC

    // Encrypted code
    var charCounterEncryptedCode = document.getElementById("charCounterEncryptedCode");   
    var OTPEncryptedCodeTextarea = document.querySelector('#OTPEncryptedCodeTextarea');
    const countCharactersEncryptedCode = () => {
        let numOfEnteredChars = OTPEncryptedCodeTextarea.value.replace(/\s/g,'').length;
        charCounterEncryptedCode.textContent = numOfEnteredChars;
    };
    OTPEncryptedCodeTextarea.addEventListener("input", countCharactersEncryptedCode);

    // Numeric key (OTP)
    var charCounterOTPNumericDecrypt = document.getElementById("charCounterOTPNumericDecrypt");   
    var OTPNumericDecryptTextarea = document.querySelector('#OTPNumericDecryptTextarea');
    const countCharactersOTPNumericDecrypt = () => {
        let numOfEnteredChars = OTPNumericDecryptTextarea.value.replace(/\s/g,'').length;
        charCounterOTPNumericDecrypt.textContent = numOfEnteredChars;
    };
    OTPNumericDecryptTextarea.addEventListener("input", countCharactersOTPNumericDecrypt);
})();


