
// *****************
// SYMMETRIC CYPHER
// *****************

(function() {

    "use strict";

    // ----------------
    // USEFUL FUNCTIONS
    // ----------------

    // https://cryptojs.gitbook.io/docs/
    // AES 256 bits key
    function createSymKey(passphrase, salt) {
        var wordArraySalt = CryptoJS.enc.Hex.parse(salt);
        var symKey = CryptoJS.PBKDF2(passphrase, wordArraySalt, {
            keySize: 256 / 32,
            iterations: 100000,
            hasher: CryptoJS.algo.SHA256
        });

        return symKey;
    };

    // encrypt AES
    function encryptCryptoJS(message, key) {
        // parse hex key
        const keyParsed = CryptoJS.enc.Hex.parse(key);
        
        // create random IV (Initialization Vector) - Reminder: 128 bits block (16 bytes) for 256 bits key
        const CRYPTO_IV_LENGTH = 16
        const iv = CryptoJS.lib.WordArray.random(CRYPTO_IV_LENGTH);
   
        // encrypt
        var encrypted = CryptoJS.AES.encrypt(message, keyParsed, {
            keySize: 256, 
            iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        // prepare IV attachment to encryptedMessage to enable further decryption
        const cipherString = CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
        
        // return `${iv.toString(CryptoJS.enc.Hex)}:${cipherString}`;
        return `${iv.toString(CryptoJS.enc.Base64)}:${cipherString}`;
    }

    // decrypt AES
    function decryptCryptoJS(encrypted, key) {
        // split IV and encrypted message
        const [ivBase64, cipher] = encrypted.split(':');
        const decrypted = CryptoJS.AES.decrypt(cipher, CryptoJS.enc.Hex.parse(key), {
            keySize: 256, 
            iv: CryptoJS.enc.Base64.parse(ivBase64),
            mode: CryptoJS.mode.CBC
        });

        return decrypted.toString(CryptoJS.enc.Utf8);
    }

    // / END - USEFUL FUNCTIONS 
    // ------------------------


    // ----------------------
    // 4 TABS
    // ----------------------
    var styleCover = 'position:absolute;width:100%;height:100%;opacity:0.3;z-index:100;background:#fff;';
    
    // ----------------------------------
    // TAB 1 - KEY GENERATION AND RECOVER
    // ----------------------------------

    /*------------------
    Show Password asym.
    -------------------*/

    (function($) {

        "use strict";
    
        // SYM KEY
        $("#show_password_sym").click(function() {
            if ($('#show_password_sym').hasClass('bi-eye-fill')) {
                $('#passwordToCreateKey').attr('type', 'text');
                $('#show_password_sym').removeClass('bi-eye-fill').addClass('bi-eye-slash-fill');
            }
            else {
                $('#passwordToCreateKey').attr('type', 'password');
                $('#show_password_sym').removeClass('bi-eye-slash-fill').addClass('bi-eye-fill');
            }
        });

        // SYM KEY RECOVER
        $("#show_password_sym_recover").click(function() {
            if ($('#show_password_sym_recover').hasClass('bi-eye-fill')) {
                $('#findBackKeyPassphrase').attr('type', 'text');
                $('#show_password_sym_recover').removeClass('bi-eye-fill').addClass('bi-eye-slash-fill');
            }
            else {
            $('#findBackKeyPassphrase').attr('type', 'password');
                $('#show_password_sym_recover').removeClass('bi-eye-slash-fill').addClass('bi-eye-fill');
            }
        });
    
    })(window.jQuery);

    // counter password characters
    (function() {
        var displayEnteredCharSymPwd = document.getElementById("displayEnteredCharSymPwd");  
        var variousContent = document.getElementById("passwordToCreateKey");   
        const symPwdCharCounter = () => {
            let numOfEnteredChars = variousContent.value.length;
            displayEnteredCharSymPwd.textContent = numOfEnteredChars;
        };
        variousContent.addEventListener("input", symPwdCharCounter);
    })();

    function keyPromiseSuccess() {
        var elem = document.body;
        elem.style.cssText = '';
        
        var elemSalt = document.getElementById('salt');
        elemSalt.removeAttribute('disabled', '');

        var elemGeneratedKeyLocation = document.getElementById('generatedKeyLocation');
        elemGeneratedKeyLocation.removeAttribute('disabled', '');
    }

    function keyPromiseError() {
        document.getElementById("errorSymGenerateKey").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Un problème est survenu lors de la génération de clé !</p>'
        return;
    }           

    function keyGeneration() {
        try {
            var passwordToCreateKey = document.getElementById('passwordToCreateKey').value;
            var saltToCreateKey = document.getElementById('salt').value;

            var symKeyGenerated = createSymKey(passwordToCreateKey.trim(), saltToCreateKey.trim()).toString().toUpperCase();
            addText(symKeyGenerated, 'generatedKeyLocation');
        }
        catch(error) {
            document.getElementById("errorSymGenerateKey").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Erreur de génération de clé symétrique : ' + error.message + '.</p>'
            return;
        }
    }

    // button to generate the key
    var buttonGenerateKey = document.getElementById('buttonGenerateKey');
    if(buttonGenerateKey){
        buttonGenerateKey.addEventListener('click', function(e){
            // remove potentiel error displayed
            var errorSymGenerateKey = document.getElementById("errorSymGenerateKey");
            errorSymGenerateKey.innerHTML = '';

            var passwordToCreateKey = document.getElementById('passwordToCreateKey').value.trim();
            var salt = document.getElementById('salt').value.trim();
            if('' == passwordToCreateKey || passwordToCreateKey.length < 40 ) {                           
                document.getElementById("errorSymGenerateKey").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer un mot de passe ou phrase secrète d\'au moins 40 caractères pour créer votre clé symétrique.</p>';
                return;
            }

            if('' == salt) {                           
                document.getElementById("errorSymGenerateKey").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer un sel après l\'avoir généré sur l\'onglet "Entropie du sel".</p>'
                return;
            }

            const keyGeneratorPromise = new Promise((resolve, reject) => {
                var elem = document.body;
                elem.style.cssText = styleCover;

                setTimeout(() => {
                    resolve(keyGeneration());
                }, 50);
            });
            keyGeneratorPromise
            .then(keyPromiseSuccess, keyPromiseError);
        });

        // cancel
        var cancelSymGenerateKey = document.getElementById('cancelSymGenerateKey');
        if(cancelSymGenerateKey){
            cancelSymGenerateKey.addEventListener('click', function(e){
                e.preventDefault();
                var errorSymGenerateKey = document.getElementById("errorSymGenerateKey");
                errorSymGenerateKey.innerHTML = '';

                const passwordToCreateKey = document.getElementById("passwordToCreateKey");
                    passwordToCreateKey.value = '';

                const salt = document.getElementById("salt");
                    salt.value = '';

                const generatedKeyLocation = document.getElementById("generatedKeyLocation");
                    generatedKeyLocation.value = '';

                removeDisabled('buttonGenerateKey');
                setDisabled('generatedKeyLocation');
            })
        }
    }


    // ----------------------
    // TAB 2 - ENCRYPT
    // ----------------------

    // button to encrypt
    var buttonToEncrypt = document.getElementById('buttonToEncrypt');
    if(buttonToEncrypt){
        buttonToEncrypt.addEventListener('click', function(e){

            var errorSymEncrypt = document.getElementById("errorSymEncrypt");
            errorSymEncrypt.innerHTML = '';

            var key = document.getElementById('symEncryptKeyLocation').value;
            var messageToEncrypt = document.getElementById('message').value;

            if('' == key) {                           
                document.getElementById("errorSymEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer votre clé symétrique.</p>'
                return;
            }

            if('' == messageToEncrypt) {                           
                document.getElementById("errorSymEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer votre contenu textuel à chiffrer.</p>'
                return;
            }

            if(!isHex(key) || key.length != 64) {                           
                document.getElementById("errorSymEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer une clé symétrique au format hexadécimal et faisant 64 caractères (256 bits).</p>'
                return;
            }

            try {           
                // Encrypt
                var encrypted = encryptCryptoJS(messageToEncrypt, key).toString();

                addText(encrypted, 'messageEncrypted');
                removeDisabled('messageEncrypted');
            }
            catch(error) {
                document.getElementById("errorSymEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Erreur de chiffrement symétrique : ' + error.message + '.</p>'
                return;
            }

        });

        // cancel
        var cancelSymEncrypt = document.getElementById('cancelSymEncrypt');
        if(cancelSymEncrypt){
            cancelSymEncrypt.addEventListener('click', function(e){
                e.preventDefault();
                var errorSymEncrypt = document.getElementById("errorSymEncrypt");
                errorSymEncrypt.innerHTML = '';

                const symEncryptKeyLocation = document.getElementById("symEncryptKeyLocation");
                    symEncryptKeyLocation.value = '';

                const message = document.getElementById("message");
                    message.value = '';

                const messageEncrypted = document.getElementById("messageEncrypted");
                    messageEncrypted.value = '';

                removeDisabled('buttonToEncrypt');
                setDisabled('messageEncrypted');
            })
        }
    }


    // ----------------------
    // TAB 3 - DECRYPT
    // ----------------------

    // button to decrypt
    var buttonToDecrypt = document.getElementById('buttonToDecrypt');
    if(buttonToDecrypt){
        buttonToDecrypt.addEventListener('click', function(e){
            e.preventDefault();

            var errorSymDecrypt = document.getElementById("errorSymDecrypt");
            errorSymDecrypt.innerHTML = '';

            var key = document.getElementById('symDecryptKeyLocation').value;  
            var messageToDecrypt = document.getElementById('messageToDecrypt').value;

            if('' == key) {                           
                document.getElementById("errorSymDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer votre clé symétrique.</p>'
                return;
            }

            if(!isHex(key) || key.length != 64) {                           
                document.getElementById("errorSymDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer une clé symétrique au format hexadécimal et faisant 64 caractères (256 bits).</p>'
                return;
            }

            if('' == messageToDecrypt) {                           
                document.getElementById("errorSymDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer votre contenu textuel à déchiffrer.</p>'
                return;
            }                   

            try {           
                // Decrypt
                var decrypted = decryptCryptoJS(messageToDecrypt, key);
                addText(decrypted, 'messageDecrypted');
                removeDisabled('messageDecrypted');
            }
            catch(error) {
                document.getElementById("errorSymDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Erreur de déchiffrement symétrique : ' + error.message + '.</p>'
                return;
            }
        });

        // cancel
        var cancelSymDecrypt = document.getElementById('cancelSymDecrypt');
        if(cancelSymDecrypt){
            cancelSymDecrypt.addEventListener('click', function(e){
                e.preventDefault();
                var errorSymDecrypt = document.getElementById("errorSymDecrypt");
                errorSymDecrypt.innerHTML = '';
               
                const symDecryptKeyLocation = document.getElementById("symDecryptKeyLocation");
                symDecryptKeyLocation.value = '';

                const messageToDecrypt = document.getElementById("messageToDecrypt");
                messageToDecrypt.value = '';
                
                const messageDecrypted = document.getElementById("messageDecrypted");
                messageDecrypted.value = '';

                removeDisabled('buttonToDecrypt');
                setDisabled('messageDecrypted');
            })
        }
    }


    // --------------------------
    // TAB 4 - FIND BACK THE KEY
    // --------------------------

    function findBackKeyPromiseSuccess() {
        var elem = document.body;
        elem.style.cssText = '';
        var elemKey = document.getElementById('findBackKey');
        elemKey.removeAttribute('disabled', '');
    }

    function findBackKeyPromiseError() {
        document.getElementById("errorSymKeyBack").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Un problème est survenu lors de la tentative de récupératon de la clé symétrique !</p>'
        return;
    }

    function findBackKeyFunction() {
        try {
            var findBackKeyPassphrase = document.getElementById('findBackKeyPassphrase').value;
            var findBackKeySalt = document.getElementById('findBackKeySalt').value;
            var findBackKey = createSymKey(findBackKeyPassphrase.trim(), findBackKeySalt.trim());
            findBackKey = findBackKey.toString().toUpperCase();

            addText(findBackKey, 'findBackKey');
        }
        catch(error) {
            document.getElementById("errorSymKeyBack").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Erreur de reconstitution de clé : ' + error.message + '.</p>'
            return;
        }
    }
    
    // button to find back the key
    var buttonFindBackKey = document.getElementById('buttonFindBackKey');
    if(buttonFindBackKey){
        buttonFindBackKey.addEventListener('click', function(e){

            var errorSymKeyBack = document.getElementById("errorSymKeyBack");
            errorSymKeyBack.innerHTML = '';

            var findBackKeyPassphrase = document.getElementById('findBackKeyPassphrase').value;
            var findBackKeySalt = document.getElementById('findBackKeySalt').value;
    
            if('' == findBackKeyPassphrase) {                           
                document.getElementById("errorSymKeyBack").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer votre mot de passe ou phrase secrète.</p>'
                return;
            }

            if('' == findBackKeySalt) {                           
                document.getElementById("errorSymKeyBack").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer le sel de votre clé.</p>'
                return;
            }      

            const findBackKeyPromise = new Promise((resolve, reject) => {
                var elem = document.body;
                elem.style.cssText = styleCover;

                setTimeout(() => {
                    resolve(findBackKeyFunction());
                }, 50);
            });
            findBackKeyPromise
            .then(findBackKeyPromiseSuccess, findBackKeyPromiseError);
        });
    }

    // ---------------------------------
    // OTP Section - One Time Pad (XOR)
    // ---------------------------------

    // ------------
    // OTP - CRYPT
    // ------------
    var buttonOtpCrypt = document.getElementById('buttonOtpCrypt');
    if(buttonOtpCrypt){
        buttonOtpCrypt.addEventListener('click', function(e){
            e.preventDefault();
            var errorOtpCrypt = document.getElementById("errorOtpCrypt");
            errorOtpCrypt.innerHTML = '';

            var xorACrypt = document.querySelector('#xorACrypt').value;
            if('' == xorACrypt) {                           
                errorOtpCrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPEmpty') + '</p>'
                return;
            }

            if(!isHex(xorACrypt)) {
                errorOtpCrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.notInHexa') + '</p>'
                return;
            }

            var xorBCrypt = document.querySelector('#xorBCrypt').value;
            if('' == xorBCrypt) {                           
                errorOtpCrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.clearMessageEmpty') + '</p>'
                return;
            } 

            if(!isHex(xorBCrypt)) {
                errorOtpCrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.notInHexa') + '</p>'
                return;
            }
                  
            if(xorACrypt == xorBCrypt) {
                errorOtpCrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textToCryptAndOTPIdentical') + '</p>'
                return;
            }

            if(xorACrypt.length != xorBCrypt.length) {                           
                errorOtpCrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPLongEnough') + '</p>'
                return;
            } 
            
            try {
                var xorACryptHex = xorACrypt;
                var xorBCryptHex = xorBCrypt;
                var xorCryptResult = common.xorHex(xorACryptHex, xorBCryptHex).toUpperCase();
                
                addText(xorCryptResult, "xorCryptResult");
                setDisabled('buttonOtpCrypt');
                setDisabled('xorACrypt');
                setDisabled('xorBCrypt');
                removeDisabled('xorCryptResult');
            } 
            catch (e) {
                errorOtpCrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> One-Time-Pad : ' + e.message + '.</p>'
                return;
            }
        });

        // cancel crypt
        var cancelOtpCrypt = document.getElementById('cancelOtpCrypt');
        if(cancelOtpCrypt){
            cancelOtpCrypt.addEventListener('click', function(e){
                e.preventDefault();
                var errorOtpCrypt = document.getElementById("errorOtpCrypt");
                errorOtpCrypt.innerHTML = '';

                const xorACrypt = document.getElementById("xorACrypt");
                    xorACrypt.value = '';

                const xorBCrypt = document.getElementById("xorBCrypt");
                    xorBCrypt.value = '';

                const xorCryptResult = document.getElementById("xorCryptResult");
                    xorCryptResult.value = '';


                // reset char. counters
                var charCounterXorA = document.getElementById('charCounterXorA');
                charCounterXorA.textContent=0;
                var charCounterXorB = document.getElementById('charCounterXorB');
                charCounterXorB.textContent=0;

                removeDisabled('buttonOtpCrypt');
                removeDisabled('xorACrypt');
                removeDisabled('xorBCrypt');
                setDisabled('xorCryptResult');
            })
        }
    }

    // -------------
    // OTP - DECRYPT
    // -------------
    var buttonOtpDecrypt = document.getElementById('buttonOtpDecrypt');
    if(buttonOtpDecrypt){
        buttonOtpDecrypt.addEventListener('click', function(e){
            e.preventDefault();
            var errorOtpDecrypt = document.getElementById("errorOtpDecrypt");
            errorOtpDecrypt.innerHTML = '';

            var xorADecrypt = document.querySelector('#xorADecrypt').value;
            if('' == xorADecrypt) {                           
                errorOtpDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.OTPEmpty') + '</p>'
                return;
            }

            if(!isHex(xorADecrypt)) {
                errorOtpDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.notInHexa') + '</p>'
                return;
            }

            var xorBDecrypt = document.querySelector('#xorBDecrypt').value;
            if('' == xorBDecrypt) {                           
                errorOtpDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textToDecryptEmpty') + '</p>'
                return;
            }    

            if(!isHex(xorBDecrypt)) {
                errorOtpDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.notInHexa') + '</p>'
                return;
            }
            
            if(xorADecrypt == xorBDecrypt) {
                errorOtpDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.textToDecryptAndOTPIdentical') + '</p>'
                return;
            }

            if(xorADecrypt.length != xorBDecrypt.length) {                           
                errorOtpDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer un masque à usage unique aussi long que le message en clair à chiffrer.</p>'
                return;
            } 
            
            try {
                var xorADecryptHex = xorADecrypt;
                var xorBDecryptHex = xorBDecrypt;
                var xorDecryptResult = common.xorHex(xorADecryptHex, xorBDecryptHex).toUpperCase();
                
                addText(xorDecryptResult, "xorDecryptResult");
                setDisabled('buttonOtpDecrypt');
                setDisabled('xorADecrypt');
                setDisabled('xorBDecrypt');
                removeDisabled('xorDecryptResult');
            } 
            catch (e) {
                errorOtpDecrypt.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Déchiffrement (OTP) : ' + e.message + '.</p>'
                return;
            }
        });

        // cancel decrypt
        var cancelOtpDecrypt = document.getElementById('cancelOtpDecrypt');
        if(cancelOtpDecrypt){
            cancelOtpDecrypt.addEventListener('click', function(e){
                e.preventDefault();
                var errorOtpDecrypt = document.getElementById("errorOtpDecrypt");
                errorOtpDecrypt.innerHTML = '';

                const xorADecrypt = document.getElementById("xorADecrypt");
                    xorADecrypt.value = '';

                const xorBDecrypt = document.getElementById("xorBDecrypt");
                    xorBDecrypt.value = '';

                const xorDecryptResult = document.getElementById("xorDecryptResult");
                    xorDecryptResult.value = '';

                // reset char. counters
                var charCounterXorADecrypt = document.getElementById('charCounterXorADecrypt');
                charCounterXorADecrypt.textContent=0;
                
                var charCounterXorBDecrypt = document.getElementById('charCounterXorBDecrypt');
                charCounterXorBDecrypt.textContent=0;

                removeDisabled('buttonOtpDecrypt');
                removeDisabled('xorADecrypt');
                removeDisabled('xorBDecrypt');
                setDisabled('xorDecryptResult');
            })
        }
    }
    
    // button to convert binary key to hex
    var btnConvertSymKeyBinaryToHex = document.getElementById('btnConvertSymKeyBinaryToHex');
    if(btnConvertSymKeyBinaryToHex){
        btnConvertSymKeyBinaryToHex.addEventListener('click', function(e){
            // remove potentiel error displayed
            var errorConvertSymKeyBinaryToHex = document.getElementById("errorConvertSymKeyBinaryToHex");
            errorConvertSymKeyBinaryToHex.innerHTML = '';

            var symKeyBinary = document.getElementById('symKeyBinary').value.trim();
            if('' == symKeyBinary || symKeyBinary.length != 256 || !isNumber(symKeyBinary)) {                         
                document.getElementById("errorConvertSymKeyBinaryToHex").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer une clé de 256 bits.</p>';
                return;
            }

            var symKeyHex = document.getElementById('symKeyHex');
            symKeyHex.value = common.convertBinaryToHexadecimal(symKeyBinary).toUpperCase();
        });
    }

})();


