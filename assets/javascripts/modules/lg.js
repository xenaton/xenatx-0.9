
var resources = {
    fr: {
        translation: {
            "error": {
                "asymPasswordEmpty": "Le mot de passe de votre clé privée doit être indiqué dans l\'onglet Clés.",
                "privateKeyEmpty": "La clé privée doit être indiquée dans l\'onglet Clés.",
                "publicKeyEmpty": "La clé publique doit être indiquée dans l\'onglet Clés.",
                "asymMessageToCryptEmpty": "Merci d\'indiquer un message à chiffrer !",
                "asymMessageToUncryptEmpty": "Merci d\'indiquer un message à déchiffrer !",
                "asymPasswordMinLength": "Indiquez un mot de passe d\'au moins 16 caractères pour protéger votre clé privée.",
                "publicKeyInfoEmpty": "Indiquez une clé publique.",
                "hashWellCalculated": "L\'empreinte SHA-512 indiquée ci-dessous correspond bien à celle calculée !",
                "hashNotCorresponding": "Empreinte SHA-512 non correspondante !",
                "hexaToUTF8Empty": "Merci d\'indiquer un contenu hexadécimal à convertir au format texte UTF8 !",
                "notInHexa": "Ce contenu n\'est pas au format hexadécimal. Merci de relire votre saisie !",
                "textToHexaEmpty": "Merci d\'indiquer un contenu textuel à convertir au format hexadécimal !",
                "hexaToTextEmpty": "Merci d\'indiquer un contenu hexadécimal à convertir au format texte !",
                "textUpperOnlyEmpty": 'Merci d\'indiquer un contenu textuel en MAJUSCULES avec uniquement des chiffres et lettres latines et le seul signe point [.], ou convertissez votre contenu au format hexadécimal avec le bouton "Convertir avant transmission" !',
                "OTPUpperOnlyEmpty": "Merci d'indiquer un contenu textuel uniquement en MAJUSCULES avec que des chiffres et lettres latines et les seuls signes suivants . : ' + - = ? [] ()",
                "moduloBrackets": "Merci de vérifier que vous avez bien fermé les crochets [] indiquant l'usage de chiffres. Leur nombre est actuellement impair.",
                "moduloParenthesis": "Merci de vérifier que vous avez bien fermé les parenthèses () dans vos formules. Leur nombre est actuellement impair.",
                "textCipheredEmpty": 'Merci d\'indiquer un contenu textuel, chiffré de préférence pour la sécurité.',
                "textToMorseEmpty": 'Merci de remplir le champ avec un contenu textuel, chiffré de préférence pour la sécurité.',
                "textOrHexaEmpty": 'Merci d\'indiquer un contenu ou de le convertir en hexadécimal au préalable.',
                "transmissionProblem": 'Un problème est survenu lors de la transmission !',
                "transmitMessageBeforeHash": 'Merci de transmettre un message avant de transmettre son empreinte.',
                "hashKO": "Les empreintes ne sont pas équivalentes !",
                "contentToQRCodeEmpty": "Merci d'indiquer un contenu à transformer en QR Code.",
                "numberOfQRCodesToReadEmpty": "Merci d'indiquer un nombre, en chiffres, de QR Codes à lire.",
                "QRCodeEmpty": "Merci d'indiquer un contenu de QR Code.",
                "QRCodesEmpty": "Merci d'indiquer un contenu dans tous les champs de QR Code.",
                "tooBigForQRCode1": "La taille des informations est trop importante pour un transfert par QR Code. Elle excède ",
                "tooBigForQRCode2": " caractères. Réduisez votre message ou coupez-le en deux.",
                "passwordPrivKeyEmpty": "Le mot de passe de votre clé privée doit être indiqué.",
                "textToSignEmpty": "Merci d\'indiquer un contenu textuel à signer.",
                "privKeyEmpty": "Clé privée absente ou incorrecte.",
                "signInfoMissing": "Des informations importantes sont manquantes ou incorrectes, merci de vérifier.",
                "signatureKO": 'ATTENTION - La signature de ce contenu textuel n\'est pas valide. La clé publique indiquée n\'est peut-être pas la soeur "jumelle" de la clé privée ayant servi à la signature. Autre raison : si dans les détails suivants il est indiqué [ Signed digest did not match ] cela signifie que le contenu textuel ne correspond pas à l\'original. Il peut avoir été falsifié ou vous avez mal recopié l\'original. Détails: ',
                "xorDifferentLengths": 'Serious failure, trying to XOR bitstreams of different lengths! ',
                "OTPEmpty": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP).',
                "OTPNumericEmpty": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP).',
                "OTPNumericNotMultiple5Block": 'Merci d\'indiquer une clé numérique composée d\'un certain nombre de chiffres qui est multiple de 5, afin de compléter automatiquement le dernier bloc de 5 chiffres par des points, si besoin. Veuillez relire les explications pour plus de précisions.',
                "clearMessageEmpty": 'Merci d\'indiquer un message en clair à chiffrer.',
                "clearCodeEmpty": 'Merci d\'indiquer un code en clair à chiffrer.',
                "encryptedCodeEmpty": 'Merci d\'indiquer un contenu chiffré à déchiffrer.',
                "clearCodeNotNumber": 'Merci d\'indiquer un code en clair sous forme de chiffres exclusivement. Prenez le temps de relire les explications, la sécurité de vos communications est en jeu.',
                "encryptedCodeNotNumber": 'Merci d\'indiquer un contenu chiffré sous forme de chiffres exclusivement. Prenez le temps de relire les explications.',
                "OTPNumericNotNumber": 'Merci d\'indiquer un code en clair et une clé numérique sous forme de chiffres exclusivement. Prenez le temps de relire les explications.',

                "textToCryptAndOTPIdentical": 'L\'OTP et le message à chiffrer ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "clearCodeAndOTPIdentical": 'Le code en clair et la clé OTP numérique ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "encryptedCodeAndOTPIdentical": 'Le contenu chiffré et la clé numérique (masque - OTP) ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "textToDecryptAndOTPIdentical": 'L\'OTP et le message à déchiffrer ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "OTPLongEnough": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP) aussi longue que le message en clair à chiffrer.',
                "OTPNumericLongEnough": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP) aussi longue que le code en clair à chiffrer.',
                "OTPNumericEncryptedCodeLongEnough": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP) aussi longue que le contenu chiffré à déchiffrer.',
                "textToDecryptEmpty": 'Merci d\'indiquer un message à déchiffrer.',
                "convertTextToHexa": "Merci d'indiquer un contenu textuel à convertir au format hexadécimal !",
                "bracketsMissing": "Il manque des crochets [] pour indiquer l'utilisation de chiffres. Sauvegardez votre contenu en le sélectionnant puis en le copiant et finalement appuyez sur Annuler pour recommencer votre saisie.",
                "bracketsMissingForSpace": "Vous ne pouvez pas utiliser d'espace dans un contenu figuratif comprenant des chiffres ou signes. Sauvegardez votre contenu en le sélectionnant puis en le copiant et finalement appuyez sur Annuler pour recommencer votre saisie.",
                "doubleSpaces": "Vous ne pouvez pas utiliser deux espaces à la suite. Sauvegardez votre contenu en le sélectionnant puis en le copiant et finalement appuyez sur Annuler pour relire et ajuster votre saisie après le caractère "
            },
            "various": {
                "blockOf4": "Présentation par bloc de 4 caractères",
                "indicateChainToHash": "Merci d\'indiquer une chaîne de caractères pour en calculer l\'empreinte.",
                "hashName": "Empreinte",
                "hashOK": "Les empreintes sont bien équivalentes. La transmission s\'est déroulée correctement.'",
                "indicateHashToCompare": "Merci d\'indiquer deux empreintes à comparer.",
                "signatureOK": 'Signature vérifiée et valide. Ce contenu textuel a bien été signé par la clé privée soeur "jumelle" de cette clé publique dont l\'identifiant est',
                "signatureVerification": 'Vérification de signature : ',
            },
            "meta": {
                "title" : "XENATON - XENATX - Application autonome"
            },
            "nav": {
                "dashboard" : "Tableau de bord",
                "asymCypher" : "Chiffr. Asym.",
                "signature" : "Signature",
                "symCypher" : "Chiffr. Sym.",
                "otpCypher" : "OTP",
                "tools" : "Outils"
            },
            "dashboard": {
                "title": "Tableau de bord",
                "introduction": `<p>              
                       Le XENATX fonctionne comme un site web, dans un navigateur, mais sans besoin d'être connecté à Internet. Le XENATX est donc une application autonome utilisable pleinement avec les trois systèmes Windows, MacOS, Linux. Quelques restrictions d'usage pour iOS sur iphone. Pour Android, une app est en cours de développement.
                       </p>
                       <p>
                       Pour la génération de clés, le chiffrement et le déchiffrement, le XENATX doit être utilisé de préférence sur un équipement dit OFF donc non connecté et jamais re-connecté.
                    </p>`,               

                "menuCypherAsym": "Chiffrement asymétrique et génération de clés",
                "menuCypherAsymKeyInfo": "Informations sur une clé publique",
                "menuSignature": "Signature et vérification de signature",
                "menuCypherSym": "Chiffrement symétrique et génération d'une clé",
                "menuOTP": "Chiffrement par masque à usage unique (OTP)",
                "menuQRCode": "QR Code - Transfert optique sécurisé",
                "menuTAM": "TAM - Morse - Transfert sonore sécurisé",
                "menuHash": "Calcul d'empreinte (hash)",
                "menuConverterHexa": "Convertisseur hexadécimal",
                "menuCodeSecurity": "Sécurité du code",

                "paragraph": `<p>
                    Découvrez comment vérifier le code du XENATX en suivant les consignes indiquées à la section <a href="#scrollSpyTools">Outils</a> dans l'onglet Sécurité du code.
                </p>
                <p>
                    Rendez-vous également à la section <a href="#scrollSpyTools">Outils</a> dans l'onglet <mark><i class="bi bi-patch-question"></i> XENATON</mark> pour en savoir plus sur l'historique et les services de XENATON.
                </p>`
            },

            "menus": {
                "menuCypherAsym": "Chiffrement asymétrique",
                "menuSign": "Signature cryptographique",
                "menuCypherSym": "Chiffrement symétrique",
                "menuCypherOTP": "OTP - Masque à usage unique",
                "menuTools": "Outils - Transfert sécurisé",
            },
                
            "cypherAsym": {
                "introduction": "Le chiffrement à clé publique permet d'éviter d'échanger une clé au préalable.",
                "menuExplainKeys": "Explications",
                "menuCreateKeys": "Créer une paire de clés",
                "menuKeys": "Clés",
                "menuEncrypt": "Chiffrement",
                "menuDecrypt": "Déchiffrement",
                "menuKeyInfo": "Info. sur une clé publique",

                "createKeys": "Créer une paire de clés",
                "paragraph01": `Créez votre paire de clés publique et privée (ECC-25519) puis&nbsp;:
                <ul>
                    <li>Stockez-les en sécurité dans un gestionnaire de mot de passe comme Keepass placé sur une clé USB ou microSD.</li>
                    <li>Stockez-y également votre mot de passe de clé privée, si possible en ne stockant pas tout le mot de passe et en retenant une partie.</li>
                    <li>Stockez le certificat de révocation dans un fichier Keepass différent et sur un autre support, si possible.</li>
                    <li>Les informations non critiques comme l'identifiant de clé publique peuvent être retrouvées dans l'onglet "Info. sur une clé publique".</li>
                </ul>`,
                "paragraph02": `Pour atteindre un bon niveau de sécurité, votre paire de clés doit être générée sur un équipement dit "OFF" qui n'est pas, et ne sera plus jamais connecté à Internet.
                <br>Cet équipement OFF doit avoir les caractéristiques suivantes&nbsp;: ni carte wifi, ni carte bluetooth, ni carte wifi-4G externe, ni câble ethernet branché, ni ports USB accessibles à des tiers, etc. En somme, aucune capacité de connexion par ondes ou par connectique.
                <br>Seuls un clavier et une souris filaires, plus jamais reconnectés à un autre équipement, peuvent être utilisés&nbsp;: les prises USB ont en effet un firmware facilement infectable permettant des fuites de données. Votre clé privée ou autres données sensibles pourraient être récupérées plus facilement que vous ne le pensez.`,
                "passphraseLabel": `Mot de passe protégeant votre clé privée`,
                "passphraseHelp": `16 caractères au minimum, mais plutôt 40 et plus, dont des spéciaux et bien sûr des chiffres, majuscules et minuscules. Attention, il n'y a aucun moyen de retrouver votre mot de passe si vous l'oubliez.`,
                
                "keyPairIntroduction": "Pour créer une clé publique anonyme, nous vous conseillons de laisser ces nom et email factices ci-dessous. Le fait qu'il y ait un nom et un email augmente la compatibilité avec certains dépôts publics de clés.",
                "keyPairName": "Nom",
                "keyPairEmail": "Email",
                "keyValidity": "Durée de validité",
                "keyPairPubKey": "Nouvelle clé publique",
                "keyPairPubKeyCopy": "Copier",
                "keyPairPrivKey": "Nouvelle clé privée",
                "keyPairPrivKeyCopy": "Copier",
                "keyPairRevCert": "Certificat de révocation",
                "keyPairRevCertCopy": "Copier",
                "keyPairID": "Identifiant de la clé publique",
                "keyPairIDHelp": "Une fois l'identifiant généré, notez-le. Vous pourrez toutefois le retrouver dans l'onglet",
                "keyPairIDInfoKey": "Info. sur une clé publique.",
                "keyPairCreateYourKeys": "Créer vos clés",
                "keyPairCancel": "Annuler",

                "keys": "Clés",
                "keysIntroduction": `Déposez ci-dessous votre clé privée et la clé publique pour pouvoir chiffrer, déchiffrer, signer ou vérifier une signature.
                <br>Vous devez avoir votre propre paire de clés ou la générer dans l'onglet`,
                "keysCreateKeysTab": "Créer une paire de clés",
                "keysPassword": "Mot de passe protégeant votre clé privée",
                "keysPubKey": "Clé publique",
                "keysPubKeyHelp": `Pour un chiffrement, indiquez la clé publique du destinataire (ou votre propre clé publique pour un message chiffré auto-adressé). Pour déchiffrer, indiquez celle de l'expéditeur pour vérifier sa signature (ou votre propre clé pour vérifier votre propre signature).`,
                "keysPrivKeyHelp": `Votre clé privée est nécessaire pour signer ou déchiffrer.`,
                "keysPubKeyLoad": `Chargez la clé publique depuis un fichier.`,
                "keysPrivKey": "Ma clé privée",
                "keysPrivKeyLoad": `Chargez votre clé privée depuis un fichier.`
            },

            "codeSecurity": {
                "title": "Sécurité du code",
                "general" : {
                    "subtitle": "Généralités",
                    "text" : `<p>       
                                    Tout utilisateur du XENATX est en droit, voire en devoir, de se demander ce qui garantit la sécurité et l'innocuité du code.
                                </p>
                                <p>  
                                    De notre point de vue, la réponse la plus valable est que les parties critiques du code proviennent de bibliothèques Open Source. Nous les utilisons sans les modifier et nous vous indiquons comment le vérifier.
                                </p>
                                <p>  
                                    Cependant, même si nous vous garantissons que vous pouvez avoir confiance, nous préconisons pour votre sécurité de... ne pas nous faire confiance... <br>Autrement dit, procédez toujours à la vérification du code du XENATX.
                                    <br>Pourquoi ? Parce que même si nous n'avons pas mis de porte dérobée, vous ne devez pas nous croire sur parole. Par ailleurs, des attaques sophistiquées ont permis la modification de code à la volée, parfois même uniquement pour certains utilisateurs en fonction de leur "fingerprint" (empreinte multi-factorielle de leur équipement informatique). 
                                </p>
                                <p>
                                    Le code du XENATX peut donc être intercepté et modifié par un tiers, au vol en quelque sorte, durant le chemin entre notre serveur et votre équipement, lors du téléchargement. Tout est possible quand vous êtes relié à Internet.
                                </p>
                                </p>
                                    Cette application est destinée à fonctionner, de préférence, sur un équipement hors ligne. Ainsi, s'il y avait une porte dérobée dans le XENATX, ce qui n'est pas le cas mais pourrait l'être par voie détournée comme vu ci-dessus, elle ne pourrait pas être exploitée, car aucun lien filaire ou par ondes n'existe avec votre équipement faisant fonctionner le XENATX. 
                                    <br>Nous nommerons tout équipement hors ligne avec le qualificatif "OFF" pour le distinguer d'un équipement "ON", connecté à Internet. 
                                    <br>Un OFF n'a donc pas de wifi, pas de bluetooth et pas de liaison filaire ethernet. Il n'y a donc pas de possibilité de fuite de données sensibles comme une clé privée, un message en clair avant chiffrement, un mot de passe, etc.
                                </p>
                                <p>
                                    Après téléchargement, il vous restera à vérifier que les bibliothèques open source, n'ont pas été modifiées par nous ou "à la volée", comme indiqué précédemment, pour affaiblir par exemple leur qualité cryptographique. Vous calculerez l'empreinte globale du XENATX (vu au premier paragraphe) pour la comparer à celle que nous indiquons sur le site <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blank">https://gitlab.com/xenaton/xenatx/-/wikis/version</a> ou sur Telegram <a href="https://t.me/xenaton_official" target="_blank">https://t.me/xenaton_official</a> mais idéalement aussi les empreintes de chaque bibliothèque open source. Nous expliquons comment faire dans le paragraphe ci-dessous "Bibliothèques Open Source".
                                </p>
                                <p>
                                    Vous l'aurez compris, nous préconisons, avec force, l'usage du XENATX sur un équipement OFF (comme le XENATOFF en cours de développement). Les QR Codes ou le TAM (Morse) servent alors au transfert sécurisé sans contact de données, depuis votre équipement OFF vers votre équipement ON. Vous pouvez ainsi transférer tous types de données comme des messages chiffrés, des clés publiques, des signatures, etc.
                                </p>
                                <p>
                                    Ces voies sécurisées vous évite l'utilisation d'une clé USB pour le transfert de données d'un équipement à un autre. Sans parler du wifi ou bluetooth dont la circulation double sens des données est invérifiable pour le grand public, voire pour tout le monde (cf. l'histoire de Crypto AG rappelée à la section Outils/XENATON).
                                    <br>Une clé USB peut facilement être infectée par un malware (via la faille badUSB entre autres) se cachant dans le firmware. Le malware pourrait alors se propager d'un équipement à un autre sans que vous puissiez vous en rendre compte. Il risquerait ensuite au prochain transfert vers votre équipement ON de faire fuiter vos données sensibles stockées sur votre équipement OFF. Tous les efforts de protection par chiffrement seraient anéantis.
                                </p>
                                <p>
                                    La conception de cette suite d'outils de Cyber Sécurité Personnelle intégrant dès l'origine un fonctionnement mi ON mi OFF, avec transferts sans contact, vous garantit le concept de "Privacy by design", sans besoin d'être un expert en sécurisation d'équipement informatique. 
                                </p>
                                <p>
                                    Le couplage du XENATX avec les services en ligne XenaTeam, XenaTrust et XenaTraining n'est pas indispensable mais ces services sont complémentaires. 
                                    <br>Si vous faites partie d'associations de défense des libertés, vous pouvez peut-être accéder à ces services gratuitement, sinon demander à votre association de nous contacter pour un partenariat.
                                </p>       
                                <p>
                                    Dans le cas de l'utilisation de notre service XenaTrust, les messages chiffrés déposés dans la DLB (Dead Letter Box - boîte aux lettres morte) sont indéchiffrables par nos serveurs. En effet, votre clé privée servant au déchiffrement reste toujours sur votre équipement ON, voire mieux, sur votre équipement OFF, ou encore mieux sur une clé USB ou microSD, elle-même chiffrée, et exclusivement utilisée sur votre équipement OFF quand cela est nécessaire.
                                </p>
                                <p>
                                    Nous ne stockons donc jamais vos clés privées et n'essayons pas d'y accéder. Physiquement, nous ne le pourrions même pas si vous utilisez un équipement OFF. Idem pour les données de types clé symétrique, mot de passe et sel utilisées à la section <mark><i class="bi bi-file-earmark-lock"></i> Chiffrement symétrique</mark>.
                                </p>
                                <p>
                                    Vous constaterez qu'aucune fonction JavaScript AJAX n'est là pour rapatrier discrètement des données sensibles vers un quelconque serveur <i>(rappel&nbsp;: si vous utilisez un équipement OFF une fuite n'est physiquement même pas possible)</i>. 
                                    <br>Vous devez bien sûr avoir quelques compétences en informatique pour le vérifier. Le code est néanmoins relativement simple. Le travail difficile étant fait par les bibliothèques open source co-créées et maintenues par des dizaines de développeurs.
                                </p>
                                `
                },
                "licence" : {
                    "subtitle" : "Licence",
                    "text" : `<p>
                                L'usage est gratuit et sans limitation. La licence a été maintenue "propriétaire" juste le temps de l'affinage de cette jeune application, encore en version alpha, avant de la faire vivre pleinement au sein de la communauté du logiciel libre. Dans le XENATX, seuls les éléments de design, d'ergonomie et d'implémentation du code non critique sont réellement soumis à notre droit de propriété intellectuelle.
                            </p>
                            <p>    
                                Le code est d'ores et déjà ouvert et volontairement non obfusqué, donc parfaitement vérifiable et améliorable. 
                                <br>Nous indiquons d'ailleurs la méthodologie détaillée pour cette vérification et encourageons à la faire. Rendez-vous pour cela sur l'onglet <mark><i class="bi bi-file-code"></i> Sécurité du code</mark> de cette présente section <a class="" href="#scrollSpyTools">Outils</a>.
                            </p>
                            <p>
                                Nous attirons votre attention sur le fait qu'aucune garantie n'est fournie par XENATON quant à l'usage du XENATX qui se fait sous la seule reponsabilité de l'utilisateur. 
                                <br>Des informations complémentaires sur ce sujet sont indiquées dans le fichier README.txt à la racine du dossier XENATX téléchargé. 
                            </p>`
                }, 
                "resilience" : {
                    "subtitle" : "Résilience",
                    "text" : `<p>
                                Concernant nos services en ligne, si nous devions "faire faillite", a minima le XENATX restera utilisable. Il s'utilise de façon autonome sur votre équipement informatique.
                            </p>
                            <p>
                                Par ailleurs toutes les notions apprises, grâce aux explications dans le XENATX et à notre programme de formation XenaTraining en ligne, resteront pour vous des acquis valables et réutilisables partout. Ces connaissances concernent des protocoles ouverts et des techniques et connaissances logiques.
                            </p>
                            <p>
                                Les clés de chiffrement et les signatures créées avec le XENATX, traduisant la lente construction d'un réseau de confiance, resteront pleinement valides, gratuites et réutilisables facilement dans d'autres systèmes. Ces clés et signatures reposant également sur des protocoles et des standards ouverts et éprouvés.
                            </p>`     
                },
                "library" : {
                    "subtitle" : "Bibliothèques Open Source",
                    "text" : `<p>
                                Nous avons listé toutes les bibliothèques JavaScript utilisées. Vous pouvez les retrouver dans le dossier assets/javascripts/libs dans le dossier source XENATX que vous avez dézippé. Vous pouvez également vérifier le contenu de nos codes JS dans assets/javascripts/modules. Nos codes JS ne sont volontairement pas minifiés, ni obfusqués. Nous indiquons les empreintes sur https://gitlab.com/xenaton/xenatx/-/wikis/version pour que vous puissiez vérifier que rien n'a été altéré par un MITM (Man In The Middle) ou autres techniques.
                            </p>
                            <p>    
                                Utilisez l'onglet <mark><i class="bi bi-hash"></i> Empreinte - Hash</mark> pour calculer les empreintes SHA-512 et les comparer entre elles. Par sécurité, doublez la vérification des empreintes de la façon indiquée plus haut dans l'encadré. 
                                <br>Merci de bien vérifier deux fois avant de penser à nous contacter si les empreintes diffèrent pour l'une ou l'autre des bibliothèques ci-dessous. Il suffit d'un commentaire laissé ou d'un retour à la ligne en trop pour rendre les empreintes différentes.
                            </p>
                            <div class="alert alert-info">
                                Nous avons implémenté et agrémenté de nos propres codes les bibliothèques suivantes pour créer un système complet et original par son concept. Cependant le premier des mérites revient bien aux créateurs et contributeurs de ces bibliothèques, et, en cascade, à tous ceux qui les ont inspirés. Pour certaines, ces bibliothèques constituent des briques essentielles du XENATX.
                                <hr>
                                <i class="bi bi-hand-thumbs-up"></i> Remerciements particuliers à davidshimjs Sgnamin Shim (qrcode.js),  Fabian Kurz (jscwlib.js - JS Morse Code Library), Joshua M. David (OTP functions), Stephen C. Philips (Morse Pro) et Julian Fietkau (Mozaic visual hash).
                            </div>
                            <p>
                                <strong>Modernizr</strong>
                                <br>Source publique&nbsp;: https://modernizr.com/download/?touchevents-setclasses-shiv (custom build)
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/modernizr-3.6.0.min.js
                            </p>
                            <p>
                                <strong>jQuery</strong>
                                <br>Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/jquery-2.2.4.min.js
                            </p>
                            <p>
                                <strong>i18next</strong>
                                <br>Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/i18next/21.8.14/i18next.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/i18next-21.8.14.min.js
                            </p>
                            <p>
                                <strong>Boostrap</strong>
                                <br>Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/js/bootstrap.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/bootstrap-5.2.0.min.js
                            </p>
                            <p>
                                <strong>Crypto-js</strong>
                                <br>Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/crypto-js-4.1.1.min.js
                            </p>
                            <p>
                                <strong>Forge</strong>
                                <br>Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/forge/1.3.1/forge.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/forge-1.3.1.min.js
                            </p>
                            <p>
                                <strong>Openpgp</strong>
                                <br>Source publique&nbsp;: https://unpkg.com/openpgp@5.3.1/dist/openpgp.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/openpgpjs-5.3.1.min.js
                            </p>
                            <p>
                                <strong>Qrcode</strong>
                                <br>Source publique&nbsp;: https://cdn.jsdelivr.net/gh/davidshimjs/qrcodejs@gh-pages/qrcode.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/qrcode.min.js
                            </p>
                            <p>
                                <strong>Chroma</strong>
                                <br>Source publique&nbsp;: https://raw.githubusercontent.com/gka/chroma.js/main/chroma.min.js
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/chroma-2.4.2.min.js
                            </p>
                            <p>
                                <strong>Js-cookie</strong>
                                <br>Source publique&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js (https://github.com/js-cookie/js-cookie/releases/tag/v2.1.4)
                                <br>Appel dans le head du fichier index.html : assets/javascripts/libs/js-cookie-2.1.4.min.js
                            </p>
                            <p>
                                <strong>Décodage morse - Audio-decoder-adaptive</strong>
                                <br>Source publique&nbsp;: https://morsecode.world/js/audio-decoder-adaptive.96da976088f5fb7c78e537b303a0ebee.js 
                                <br>Si la chaine suivante entre crochets du lien ci-dessus change [ 96da976088f5fb7c78e537b303a0ebee ].js, copiez et collez juste le contenu https://morsecode.world/js/audio-decoder-adaptive.*.js et collez-le localement en remplacement. 
                                <br>Les différences ne devraient pas être grandes et vous êtes ainsi garantis d'utiliser une source publique non "manipulée". La bibliothèque complète est ici, pour étude : https://github.com/scp93ch/morse-pro
                                <br>Appel en pied de page du fichier index.html : assets/javascripts/libs/audio-decoder-adaptive-2.0.min.js
                            </p>
                            <p>
                                <strong>Encodage morse - jscwlib.js</strong>
                                <br>ATTENTION ! Nous avons opéré quelques changements mineurs sur cette bibliothèque. Lisez le fichier README.txt à la racine du dossier du XENATX pour voir les changements et calculer au mieux l'empreinte pour vérification.
                                <br>Source publique&nbsp;: https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/
                                <br>Appel dans le head du fichier index.html&nbsp;: assets/javascripts/libs/jscwlib.js
                            </p>
                            <p>
                                <strong>Empreinte visuelle - mosaicVisualHash-1.0.1.js</strong>
                                <br>Source publique&nbsp;: https://raw.githubusercontent.com/jfietkau/Mosaic-Visual-Hash/master/mosaicVisualHash.js
                                <br>Appel dans le head du fichier index.html&nbsp;: assets/javascripts/libs/mosaicVisualHash-1.0.1.js
                            </p>`        
                },
                "codesjs" : {
                    "subtitle" : "Codes JavaScript",
                    "text" : `<p>
                                Consultez le site <a href="https://gitlab.com/xenaton/xenatx" target="_blank">https://gitlab.com/xenaton/xenatx</a> pour comparer tous les fichiers des codes internes JS suivants&nbsp;:
                            </p>
                            <p>
                                <strong>Asym. Cypher</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/asym-cypher.js
                            </p>
                            <p>
                                <strong>Asym. Generate Keys</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/asym-generate-keys.js
                            </p>
                            <p>
                                <strong>Asym. Info Key</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/asym-info-key.js
                            </p>
                            <p>
                                <strong>Core</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/core.js
                            </p>
                            <p>
                                <strong>Hash</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/hash.js
                            </p>
                            <p>
                                <strong>Hash File</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/hash-file.js
                            </p>
                            <p>
                                <strong>Hex converter</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/hex-converter.js
                            </p>
                            <p>
                                <strong>i18n</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/i18n.js
                            </p>
                            <p>
                                <strong>Image converter</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/image-converter.js
                            </p>
                            <p>
                                <strong>Lg</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/lg.js
                            </p>
                            <p>
                                <strong>Morse</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/morse.js
                            </p>
                            <p>
                                <strong>OTP</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/otp-numeric.js
                            </p>
                            <p>
                                <strong>QR Code</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/qrcode.js
                            </p>
                            <p>
                                <strong>Signature</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/signature.js
                            </p>
                            <p>
                                <strong>Sym. cypher</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/sym-cypher.js
                            </p>
                            <p>
                                <strong>Sym. entropy</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/sym-entropy.js
                            </p>
                            <p>
                                <strong>Various functions</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/various-functions.js                               
                            </p>`        
                },
                "conclusion" : {
                    "subtitle" : "Conclusion",
                    "text" : `<p>
                                Nous vous remercions de votre confiance, non aveugle comme il se doit. Soyez certains que nous avons le respect de la vie privée et de la souveraineté personnelle chevillé au corps. Découvrez-en plus sur les services de XENATON dans cette section <a class="" href="#scrollSpyTools">Outils</a> sur l'onglet <mark><i class="bi bi-patch-question"></i> XENATON</mark>.
                            </p>
                            <p>
                                Pour augmenter encore la sécurité fournie par le XENATX et pallier de potentielles fragilités du chiffrement (manque d'entropie des clés, IA, timing attack, calculateur quantique, etc.), plusieurs solutions s'offrent à vous et la liste n'est pas exhaustive.
                            </p>
                            <p>
                                Pour plus d'entropie, les clés peuvent être générées par des CSPRNG en ligne de commande sur des systèmes Linux/Unix. Des générateurs d'entropie additionnels peuvent aussi aider. Le plus courant est lié aux mouvements de la souris, comme celui que nous utilisons dans la section "Chiffrement sym." et l'onglet "Entropie du sel". Le but est de tendre vers le TRNG (True Random Number Generator). 
                                <br>Des logiciels open source comme Veracrypt ont ce type de solution dans leur "Keyfile generator". Par ailleurs, leur fonctionnalité de volume caché est aussi à découvrir. La Stéganographie sous toutes ses formes a également son importance. 
                            </p>
                            <p>
                                Vous pouvez également améliorer la résistance de vos contenus chiffrés par l'utilisation de masques à usage unique (OTP - One-Time-Pad) pour les élements les plus sensibles de vos messages avant "sur-chiffrement classique". Cela nécessite néanmoins l'échange préalable de répertoires d'OTP, bien générés avec un maximum d'entropie. 
                                <br>Pour augmenter l'entropie tant pour l'AES-256 que pour les masques à usage unique, nous indiquons des solutions de création de clés à base de dés de précision... Lent mais fiable. Cela restera encore longtemps la solution la plus accessible à tous et la plus sûre.
                                <br>Cela peut en valoir la peine dans certaines situations. Nous approfondissons ces sujets dans notre programme XenaTraining mais vous en avez déjà un très bon aperçu dans la rubrique "Explications" de la section "OTP - Masque à usage unique".
                            </p>
                            <p>
                                Dans tous les cas, nous vous conseillons de vous méfier grandement des failles matérielles et logicielles de vos appareils connectés en permanence à Internet... Pensez à utiliser le XENATX, ou un autre logiciel de chiffrement, sur un équipement OFF définitivement hors ligne et faites la liaison avec vos appareils ON grâce aux voies de transfert sécurisé offertes par le XENATX. Faire autrement est illusoire en terme de sécurité réelle.
                            </p>
                            <p>
                                Merci de votre intérêt, bonne utilisation éclairée et bonne reprise d'autonomie réelle.
                            </p>`        
                },
                "menuCypherAsym": "Chiffrement asym. et génération d'une paire de clés"
            }
        }
    },
    en: {
        translation: {
            "meta": {
                "title" : "XENATON - XENATX - Autonomous application"
            },
            "nav": {
                "dashboard" : "Dashboard",
                "asymCypher" : "Asym. Cypher",
                "signature" : "Signature",
                "symCypher" : "Sym. Cypher",
                "otpCypher" : "OTP",
                "tools" : "Tools"
            },
            "dashboard": {
                "title" : "Dashboard",
                "introduction": `<p>
                    XENATX works like a website, in a browser, but without the need to be connected to the Internet. Just unzip the downloaded folder and double-click on the index.html file to launch it. XENATX is a standalone application that can be used on all platforms (MacOS, Windows, Linux) with just a few restrictions for the moment on iOS and Android.
                </p>
                <p>
                    Its use, for the encryption and decryption parts, should preferably be done on a so-called OFF <i>(not connected and never re-connected)</i> equipment. The following essential features are made available to you&nbsp;:
                </p>`,

                "menuCypherAsym": "Asymmetric cypher and generating a key pair",
                "menuSignature": "Signature and signature verification",
                "menuCypherSym": "Symmetric encryption and generation of a key",
                "menuOTP": "Encryption by One-Time-Pad (OTP)",
                "menuQRCode": "QR Code - Contactless transfer",
                "menuTAM": "TAM - Acoustic Morse Transmitter",
                "menuHash": "Footprint calculation (hash)",
                "menuConverterHexa": "Hexadecimal converter",
                "menuCodeSecurity": "Code Security",

                "paragraph1": `<p>
                    Find out how to verify the code of the XENATX by following the instructions given in the <a href="#scrollSpyTools">Tools</a> section in the tab <mark><i class="bi bi-file-code"></i> Security of the code</mark>.
                </p>
                <p>
                    Also go to the <a href="#scrollSpyTools">Tools</a> section in the <mark><i class="bi bi-patch-question"></i> XENATON tab</mark> to learn more about the history and services of XENATON.
                </p>`
            },

            "menus": {
                "menuCypherAsym": "Asymmetric cypher",
                "menuSign": "Signature",
                "menuCypherSym": "Symmetric cypher",
                "menuCypherOTP": "Encryption by One-Time-Pad (OTP)",
                "menuTools": "Tools"
            },
                
            "cypherAsym": {
                "introduction": "Public key encryption avoids the need to secretly exchange a key beforehand.",
                "menuCreateKeys": "Creating a key pair",
                "createKeys": "Creating a key pair",
                "menuKeys": "Keys",
                "keys": "Keys",
                "menuEncrypt": "Encryption",
                "menuDecrypt": "Decryption",
                "menuKeyInfo": "Info. about a key",
                "menuKeyInfo": "Info. about a key",
            },

            "codeSecurity": {
                "title": "Sécurité du code",
                "general" : {
                    "subtitle": "General information",
                    "text" : `<p>
                        Every XENATX user has the right, even the duty, to ask what guarantees the security and safety of the code.
                    </p>
                     <p>  
                        From our point of view, the most valid answer is that the critical parts of the code come from open source libraries. We use them without modifying them and we tell you how to check this.
                    </p>
                    <p>  
                       Although we guarantee that you can trust us, we recommend that you do not trust us for your safety...
                       <br>In other words, always check the XENATX code.
                       <br>Why? Because even if we didn't put a backdoor in, you shouldn't take our word for it. Moreover, sophisticated attacks have allowed code modification on the fly, sometimes even only for certain users according to their "fingerprint" (multi-factor fingerprint of their computer equipment). 
                    </p>
                    <p>
                        The code of the XENATX can therefore be intercepted and modified by a third party, on the fly as it were, during the download from our server to your equipment. Anything is possible when you are connected to the Internet.
                    </p>            
                    </p>
                        This application is intended to run, preferably, on offline equipment. So if there was a backdoor in the XENATX, which is not the case but could be indirectly as mentioned above, it could not be exploited, as there is no way to reach your equipment running the XENATX.
                        <br>We will name any offline equipment with the qualifier "OFF" to distinguish it from "ON" equipment, connected to the Internet.
                        <br>An OFF has no wifi, no bluetooth and no wired ethernet link. There is therefore no possibility of leakage of sensitive data such as a private key, a message in clear before encryption, a password, etc.
                    </p>
                    <p>
                        So, after downloading, you will have to check that the open source libraries have not been modified "on the fly", as indicated previously, to weaken their cryptographic quality for example. You will calculate the global footprint of XENATX (seen in the first paragraph) to compare it to the one we indicate on the site <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blank">https://gitlab.com/xenaton/xenatx/-/wikis/version</a> or on Telegram <a href="https://t.me/xenaton_official" target="_blank">https://t.me/xenaton_official</a> but also the fingerprints of each open source library. We explain how to do this in the paragraph below "Open Source Libraries".
                    </p>
                    <p>
                        You will have understood, we strongly recommend the use of XENATX on an OFF equipment (like the XENATOFF under development) for the most sensitive communications. QR Codes or TAM (Morse) are then used for contactless transfer from your OFF equipment to your ON equipment. You can transfer all types of data such as encrypted messages, public keys, signatures, etc.
                        <br>QR codes, in addition to their practicality and speed of use, are intended to avoid the use of a USB key for data transfers from one equipment to another. Same for the TAM.
                        <br>Indeed, a USB key can easily be infected by a malware (badUSB type among others) that hides in the firmware. The malware could then spread from one device to another without you being aware of it. It could then leak your sensitive data stored on your OFF device the next time it is transferred to your ON device. All encryption protection efforts would be wiped out.
                    </p>
                    <p>
                        The design of this suite of Personal Cyber Security tools integrating from the beginning a half ON half OFF operation, with contactless transfers, guarantees you the true concept of "Privacy by design", without the need to be an expert in securing computer equipment.
                        <br>The coupling of XENATX with the online services XenaTeam, XenaTrust and XenaTraining is not essential but these services are complementary. 
                        <br>If you are a member of an advocacy organization, you may be able to access these services for free, or even have your organization contact us for a partnership.
                    </p>
                    <p>
                        When using our XenaTrust service, the encrypted messages deposited in the DLB (Dead Letter Box) cannot be decrypted by the server. Indeed, your private key used for decryption always remains on your ON device, or even better, on your OFF device, or even better on a USB or MicroSD key, itself encrypted, and exclusively used on your OFF device when needed.
                    </p>
                    <p>            
                        So we never store your private keys or try to access them. Physically, we couldn't even do that if you use OFF equipment. The same goes for the symmetric key, password and salt data used in the <mark><i class="bi bi-file-earmark-lock"></i> Symmetric encryption</mark>.
                    </p>
                    <p>
                        You will notice that no JavaScript AJAX function is there to discreetly repatriate sensitive data to any server <i>(reminder&nbsp;: if you use OFF equipment a leak is not even physically possible)</i>. 
                        <br>Of course you need some computer skills to check it. The code is relatively simple though. The hard work is done by open source libraries co-created and maintained by dozens of developers.
                    </p>
                    `
                },
                "licence" : {
                    "subtitle" : "Licence",
                    "text" : `<p>
                                For the moment, the use is free, without limitation, but the license has been kept "proprietary" just the time needed to refine this young application, still in alpha version, before making it fully live within the free software community. In XENATX, only the design, ergonomics and implementation elements of the non-critical code are really subject to our intellectual property rights.
                            </p>
                            <p>    
                                The code is already open and voluntarily unobfuscated, so it can be verified and improved. 
                                <br>We indicate the detailed methodology for this verification and encourage to do it. Go to the tab <mark><i class="bi bi-file-code"></i> Security of code</mark> of this section <a class="" href="#scrollSpyTools">Tools</a>.
                            </p>
                            <p>
                                We draw your attention to the fact that no guarantee is provided by XENATON regarding the use of XENATX which is done under the sole responsibility of the user. 
                                <br>Additional information on this topic can be found in the README.txt file at the root of the downloaded XENATX folder.
                            </p>`
                }, 
                "resilience" : {
                    "subtitle" : "Resilience",
                    "text" : `<p>
                                Concerning our online services, if we were to "go bankrupt", at least the XENATX will remain usable. It can be used autonomously on your machine ON or ideally OFF.
                            </p>
                            <p>
                                Moreover, all the notions learned, thanks to the explanations in the XENATX and to our online XenaTraining program, will remain for you valid and reusable knowledge everywhere. This knowledge is mostly about open protocols and logical techniques and knowledge.
                            </p>
                            <p>
                                Essential&nbsp;: the encryption keys and signatures created with XENATX, reflecting the slow construction of a trusted network, will remain fully valid, free of charge and easily reusable. 
                                <br>They will simply need to be reloaded into other systems. These keys and signatures are also based on open and proven protocols and standards.
                            </p>`        
                },
                "library" : {
                    "subtitle" : "Open Source Libraries",
                    "text" : `<p>
                                We have listed all the JavaScript libraries used. You can find them in the assets/javascripts/libs folder in the XENATX source folder you unzipped. You can also check the content of our JS codes in assets/javascripts/modules. Our JS codes are deliberately not minified or obfuscated. We show the fingerprints on https://gitlab.com/xenaton/xenatx/-/wikis/version so that you can verify that nothing has been tampered with by MITM (Man In The Middle) or other techniques.
                            </p>
                            <p>    
                                Use the tab <mark><i class="bi bi-hash"></i> Hash - Fingerprint</mark> to calculate the SHA-512 fingerprints and compare them to each other. To be safe, double-check the fingerprints as described above in the box. 
                                <br>Please double check before thinking about contacting us if the prints differ for any of the libraries below. All it takes is one comment left or one extra line break to make the prints different.
                            </p>
                            <p class="alert alert-info">
                                We have found, implemented and added our own code to the following libraries to create a complete and original system. However, the first merit goes to the creators and contributors of these libraries, and, in cascade, to all those who inspired them. For some of them, these libraries are essential building blocks of XENATX.
                                <br>
                                <br><i class="bi bi-hand-thumbs-up"></i> Special thanks to davidshimjs Sgnamin Shim (qrcode.js), Fabian Kurz (jscwlib.js - JS Morse Code Library), Joshua M. David (OTP functions) and Stephen C. Philips (Morse Pro).
                            </p>
                            <p>
                                <strong>Modernizr</strong>
                                <br>Public source&nbsp;: https://modernizr.com/download/?touchevents-setclasses-shiv (custom build)
                                <br>Call in the head of the file index.html : assets/javascripts/libs/modernizr-3.6.0.min.js
                            </p>
                            <p>
                                <strong>jQuery</strong>
                                <br>Public source&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/jquery-2.2.4.min.js
                            </p>
                            <p>
                                <strong>i18next</strong>
                                <br>Public source&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/i18next/21.8.14/i18next.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/i18next-21.8.14.min.js
                            </p>
                            <p>
                                <strong>Boostrap</strong>
                                <br>Public source&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/js/bootstrap.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/bootstrap-5.2.0.min.js
                            </p>
                            <p>
                                <strong>Crypto-js</strong>
                                <br>Public source&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/crypto-js-4.1.1.min.js
                            </p>
                            <p>
                                <strong>Forge</strong>
                                <br>Public source&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/forge/1.3.1/forge.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/forge-1.3.1.min.js
                            </p>
                            <p>
                                <strong>Openpgp</strong>
                                <br>Public source&nbsp;: https://unpkg.com/openpgp@5.3.1/dist/openpgp.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/openpgpjs-5.3.1.min.js
                            </p>
                            <p>
                                <strong>Qrcode</strong>
                                <br>Public source&nbsp;: https://cdn.jsdelivr.net/gh/davidshimjs/qrcodejs@gh-pages/qrcode.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/qrcode.min.js
                            </p>
                            <p>
                                <strong>Chroma</strong>
                                <br>Public source&nbsp;: https://raw.githubusercontent.com/gka/chroma.js/main/chroma.min.js
                                <br>Call in the head of the file index.html : assets/javascripts/libs/chroma-2.4.2.min.js
                            </p>
                            <p>
                                <strong>Js-cookie</strong>
                                <br>Public source&nbsp;: https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js (https://github.com/js-cookie/js-cookie/releases/tag/v2.1.4)
                                <br>Call in the head of the file index.html : assets/javascripts/libs/js-cookie-2.1.4.min.js
                            </p>
                            <p>
                                <strong>Morse code decoding - Audio-decoder-adaptive</strong>
                                <br>Public source&nbsp;: https://morsecode.world/js/audio-decoder-adaptive.96da976088f5fb7c78e537b303a0ebee.js 
                                <br>If the following string in square brackets in the above link changes
                                [ 96da976088f5fb7c78e537b303a0ebee ].js, just copy and paste the content https://morsecode.world/js/audio-decoder-adaptive.*.js and glue it locally as a replacement. 
                                <br>The differences should not be great and you are thus guaranteed to use a public source that is not "manipulated". The complete library is here, for study&nbsp;: https://github.com/scp93ch/morse-pro
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/libs/audio-decoder-adaptive-2.0.min.js
                            </p>
                            <p>
                                <strong>Morse encoding - jscwlib.js</strong>
                                <br>WARNING! We have made some changes on this library. Read the README.txt file at the root of the XENATX folder to see the changes and calculate the best footprint for verification.
                                <br>Public source&nbsp;: https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/
                                <br>Call in the head of the file index.html&nbsp;: assets/javascripts/libs/jscwlib.js
                            </p>
                            `        
                },
                "codesjs" : {
                    "subtitle" : "JavaScript codes",
                    "text" : `<p>
                                Visit the site <a href="https://gitlab.com/xenaton/xenatx" target="_blank">https://gitlab.com/xenaton/xenatx</a> to compare all the following JS internal code files&nbsp;:
                            </p>
                            <p>
                                <strong>Asym. Cypher</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/asym-cypher.js
                            </p>
                            <p>
                                <strong>Asym. Generate Keys</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/asym-generate-keys.js
                            </p>
                            <p>
                                <strong>Asym. Info Key</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/asym-info-key.js
                            </p>
                            <p>
                                <strong>Core</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/core.js
                            </p>
                            <p>
                                <strong>Hash</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/hash.js
                            </p>
                            <p>
                                <strong>Hash File</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/hash-file.js
                            </p>
                            <p>
                                <strong>Hex converter</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/hex-converter.js
                            </p>
                            <p>
                                <strong>i18n</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/i18n.js
                            </p>
                            <p>
                                <strong>Image converter</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/image-converter.js
                            </p>
                            <p>
                                <strong>Lg</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/lg.js
                            </p>
                            <p>
                                <strong>Morse</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/morse.js
                            </p>
                            <p>
                                <strong>OTP</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/otp-numeric.js
                            </p>
                            <p>
                                <strong>QR Code</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/qrcode.js
                            </p>
                            <p>
                                <strong>Signature</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/signature.js
                            </p>
                            <p>
                                <strong>Sym. cypher</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/sym-cypher.js
                            </p>
                            <p>
                                <strong>Sym. entropy</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/sym-entropy.js
                            </p>
                            <p>
                                <strong>Various functions</strong>
                                <br>Call in footer of the file index.html&nbsp;: assets/javascripts/modules/various-functions.js                               
                            </p>`        
                },
                "conclusion" : {
                    "subtitle" : "Conclusion",
                    "text" : `<p>
                                We thank you for your trust, not blindly as it should be. You can be sure that we are committed to respecting your privacy and personal sovereignty. Find out more about XENATON's services in this section <a class="" href="#scrollSpyTools">Tools</a> on tab <mark><i class="bi bi-patch-question"></i> XENATON</mark>.
                            </p>
                            <p>
                                To further increase the security provided by XENATX and to compensate for potential encryption weaknesses (lack of key entropy, AI, timing attack, quantum computing, etc.), several solutions are available and the list is not exhaustive.
                            </p>
                            <p>
                                For more entropy, keys can be generated by command line CSPRNGs on Linux/Unix systems. Additional entropy generators can also help. The most common one is related to mouse movements, like the one we use in the "Symmetric cypher" section and the "Salt entropy" tab. The goal is to move towards TRNG (True Random Number Generator). 
                                <br>Open source software like Veracrypt have this kind of solution in their "Keyfile generator". Moreover, their hidden volume functionality is also worth discovering. Steganography in all its forms is also important.
                            </p>
                            <p>
                                You can also improve the resistance of your encrypted contents by using disposable masks (OTP - One-Time-Pad) for the most sensitive elements of your messages in clear text before "classic" encryption. However, this requires the prior exchange of OTP directories, well generated with maximum entropy. To increase entropy, we even indicate solutions based on dice... Slow but reliable.
                                <br>It can be worth it in some situations. We go into more detail on these topics in our XenaTraining program, but you already have a good overview in the "Explanations" section of "OTP - One-Time-Pad".
                            </p>
                            <p>
                                In any case, we advise you to beware of hardware and zero day vulnerabilities of your operating systems and applications on your smartphones and computers permanently connected to the Internet... Seriously think about using XENATX, or another encryption software, on an OFF device that is permanently offline.
                            </p>
                            <p>
                                Thank you for your interest and good use of XENATX.
                            </p>`        
                },
                "menuCypherAsym": "Asymmetric encryption and key pair generation"
            }
        }
    }
};