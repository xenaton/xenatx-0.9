// ***************
// IMAGE CONVERTER
// ***************

(function() {

    "use strict";

    let base64String = "";

    function imageUploaded() {

        var file = document.getElementById("imageToTextFile").files[0];
        var reader = new FileReader();
        
        reader.onload = function () {
            base64String = reader.result.replace("data:", "")
                .replace(/^.+,/, "");
    
        var imageBase64Stringsep = base64String;
            read(imageBase64Stringsep);
        }
        if(file) {
            reader.readAsDataURL(file);
        }
    }
    
    function read(imageBase64Stringsep) {
        
        var texta = document.createElement("textarea");
        texta.setAttribute('id', 'texta');
        texta.setAttribute('class', 'form-control');
        texta.setAttribute('rows', '8');
        
        var textaParent = document.getElementById("textaLocation");
        textaParent.innerHTML = ''; // empty the eventual precedent textarea
        textaParent.appendChild(texta);
        
        var textarea = document.getElementById("texta");
        textarea.value = imageBase64Stringsep;
    }

    function base64ToImage() {
        var imageBase64Stringsep = document.getElementById("texta2").value;
        var errorMessage = "Merci d'insérer un contenu textuel encodé en base64.";
        if(imageBase64Stringsep) {

            if(!isBase64(imageBase64Stringsep)) {
                alert(errorMessage);
                return;
            }

            var image = document.createElement("img"),
                imageParent = document.getElementById("imageLocation");

            imageParent.innerHTML = ''; // empty the eventual precedent image
            image.id = "insertedImage";
            image.className = "insertedImage";
            // image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";
            image.src = 'data:image/png;base64,' + imageBase64Stringsep;
            imageParent.appendChild(image);
        } 
        else {
            alert(errorMessage);
        }
    }

    function handleFileSelect(evt) {
        var original = document.getElementById("original"),
            stego = document.getElementById("stego"),
            img = document.getElementById("img"),
            cover = document.getElementById("cover");

        if(!original || !stego) return;

        var files = evt.target.files; // FileList object

        // loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    img.src = e.target.result;
                    img.title = escape(theFile.name);
                    stego.className = "half invisible";
                    cover.src = "";
                    updateCapacity();
                };
            })(f);

            // read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    function updateCapacity() {
        var img = document.getElementById('img'),
            textarea = document.getElementById('text');
        if(img && text)
            document.getElementById('capacity').innerHTML = textarea.value.length + '/' + steg.getHidingCapacity(img) +' caractères';
    }
    
    function reload() {
        location.reload(true); // true option means cache will be removed (what we need)
    }

    document.getElementById('transformBtn').addEventListener('click', imageUploaded, false);
    document.getElementById('base64ToImageBtn').addEventListener('click', base64ToImage, false);
    // document.getElementById('text').addEventListener('keyup', updateCapacity, false);
    // document.getElementById('cancel').addEventListener('click', reload, false);
    updateCapacity();

})();
